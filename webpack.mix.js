const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.js(['resources/js/main.js'], 'public/js');

mix.js([
    'resources/js/slick-custom.js',
], 'public/js');


mix.styles([
    'resources/css/util.css'
], 'public/css/themes.css');

mix.styles([
    'resources/css/main.css'
], 'public/css/main.css');

mix.styles([
    'resources/css/auth.css'
], 'public/css/auth.css');

mix.browserSync({
    proxy: 'localhost:8000',
    files: [
        './resources/views/**/*.blade.php'
    ]
});

