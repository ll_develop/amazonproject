<?php

namespace App\Listeners;

use App\Http\Controllers\SegmentController;
use Exception;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Segment;

class LoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event){
        logger('Event Login');
        logger($event->user->name);
        SegmentController::segmentIdentify($event->user);
    }
}
