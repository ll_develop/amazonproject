<?php

namespace App\Listeners;

use App\Http\Controllers\SegmentController;
use App\Jobs\SendUserEmailJob;
use App\Notifications\SendWelcomeEmailNotification;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event){
        //logger($event->user);
        $emailJob = (new SendUserEmailJob($event->user))->delay(Carbon::now()->addMinute(1));
        dispatch($emailJob);
        SegmentController::segmentIdentify($event->user);
        /*
        $event->user->notify(
            new SendWelcomeEmailNotification()
        );
        */
    }
}
