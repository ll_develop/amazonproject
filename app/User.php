<?php

namespace App;


use App\Notifications\ResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'image', 'provider', 'provider_id', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function isAdmin(){
        return $this->role == 1;
    }

    public function logSearches() {
        return $this->belongsToMany('App\Model\LogSearch', 'log_searches_users')->withTimestamps();
    }

    public function logProductClicks() {
        return $this->belongsToMany('App\Model\LogProductClick', 'log_product_clicks_users')
            ->withPivot('id','code','open')
            ->withTimestamps()
            ->orderBy('id','DESC');
    }

    public function sendPasswordResetNotification($token){
        $this->notify(new ResetPasswordNotification($token,$this->name));
    }
}
