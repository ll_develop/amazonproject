<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\HelperSearch;
use App\Model\Contest;
use App\Model\LogProductClicksUser;
use App\Model\Winner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller{


    public function index(Request $request){

        if(Auth::user()){
            $pivot = LogProductClicksUser::where(['user_id' => Auth::user()->id , 'open' => true])
                ->with('logProductClicks')->orderBy('updated_at','DESC')->get()->take(5);

            $winGift = Winner::where(['user_id' => Auth::user()->id ])
                ->with('contest')->orderBy('updated_at','DESC')->get();

            $contestActive = Contest::where('active',true)->first();

            $codeGenerate = 0;
            if($contestActive){
                $myClickCode = LogProductClicksUser::where(['user_id' => Auth::user()->id ])->whereBetween('updated_at', [$contestActive->start, $contestActive->end])->get();
            }else{
                $contestActive = Contest::all()->last();
                $myClickCode = LogProductClicksUser::where(['user_id' => Auth::user()->id ])->whereBetween('updated_at', [$contestActive->end, now()])->get();
            }

            if($myClickCode){
                $codeGenerate = $myClickCode->count();
            }

            //return response()->json($myClickCode);
            //*************************************************************

            $data = [
                'productClicked' => $pivot,
                'winGift' => $winGift,
                'contestActive' => $contestActive,
                'codeGenerate' => $codeGenerate,
            ];

            //**************************************************************
            return view('user.dashboard',$data);
        }
        return redirect()->route('home');
    }

}
