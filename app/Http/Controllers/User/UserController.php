<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\LogProductClicksUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    //
    public function myCode(Request $request){
        if(Auth::user()){
            $pivot = LogProductClicksUser::where(['user_id' => Auth::user()->id , 'open' => true])
                ->with('logProductClicks')
                ->orderBy('updated_at','DESC')
                ->paginate(5);

            $data = [
                'productClicked' => $pivot
            ];

            //**************************************************************
            return view('user.my_code',$data);
        }
        return redirect()->route('home');
    }

    public function myAccount(Request $request){
        if(Auth::user()){
            $editprofile = User::where('id', Auth::user()->id)->first();
            $data = [
                'editprofile' => $editprofile
            ];
            return view('user.my_account',$data);
        }
        return redirect()->route('home');
    }


    public function myaccountEdit(Request $request){
        $data = $request->all();
        $id=Auth::user()->id;

        $rules = array(
            'name' => 'max:255',
            'address' => 'max:255',
            'phone' => 'max:255|nullable|unique:users,phone,'.$id,
        );

        $validator = Validator::make(Input::all(), $rules);

        if($validator->fails()){
            return redirect()->route('user_myaccount')
                ->withErrors($validator)
                ->withInput();
        }

        $name=$data['name'];
        $phone=$data['phone'];
        $address=$data['address'];

        $usr = User::where('id',Auth::user()->id)->first();
        $usr->name = $name;
        $usr->phone = $phone;
        $usr->address = $address;
        $usr->save();

        return back()->with('success-info', trans('msg.menu_user.my_account.success'));
    }

    public function myaccountCredential(Request $request){
        $data = $request->all();
        $id=Auth::user()->id;

        $rules = array(
            'email'=>'email|unique:users,email,'.$id,
            'name' => 'max:255',
        );

        if($request->has('password') && ($request->password != '')) {
            $rules['password'] = "string|min:6|confirmed";
        }

        $validator = Validator::make(Input::all(), $rules);

        //logger(json_encode($validator));

        if($validator->fails()){
            return redirect()->route('user_myaccount')
                ->withErrors($validator)
                ->withInput();
        }

        $email=$data['email'];
        $usr = User::where('id',Auth::user()->id)->first();

        if($data['password']!="") {
            $passtxt=bcrypt($data['password']);
            $usr->password = $passtxt;
        } else {
            //$passtxt=$data['savepassword'];
        }

        $usr->email = $email;
        $usr->save();

        return back()->with('success-cred', trans('msg.menu_user.my_account.success'));
    }
}
