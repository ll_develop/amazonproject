<?php
/**
 * Created by IntelliJ IDEA.
 * User: LeandroDev
 * Date: 24/11/2018
 * Time: 17.10
 */

namespace App\Http\Controllers;

use App\Model\LogProductClick;
use App\Model\LogProductClicksUser;
use App\Model\LogSearch;
use App\Model\ProductAmazon;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class LoggerUser{

    const LOGGER_USER_ENABLED = true;

    public static function loggerSearch($searchString){
        $cleanSearch = Helper::clean($searchString);
        $logSearch = LogSearch::where('id_string',$cleanSearch)->first();
        if(!$logSearch) {
            //New Search
            $logSearch = new LogSearch();
            $logSearch->id_string = $cleanSearch;
            $logSearch->string = $searchString;
            $logSearch->save();
        }

        if(LoggerUser::LOGGER_USER_ENABLED && $logSearch != null) {
            if (Auth::user() && !Auth::user()->isAdmin()) {
                Auth::user()->logSearches()->attach($logSearch);
            }
        }
    }

    public static function createOrFindLogProductClick($asin){
        $logProductClick = LogProductClick::where('asin',$asin)->first();

        if(!$logProductClick){
            //bew productclick
            $logProductClick = new LogProductClick();
            $logProductClick->asin = $asin;
            $logProductClick->save();
        }

        return $logProductClick;
    }


    public static function loggerClickProduct($asin){
        $logProductClick = null;

        if (Auth::user() && !Auth::user()->isAdmin()) {
            $logProductClick = LoggerUser::createOrFindLogProductClick($asin);
        }elseif(!Auth::user()){
            $logProductClick = LoggerUser::createOrFindLogProductClick($asin);
        }

        if($logProductClick != null) {
            if (Auth::user() && !Auth::user()->isAdmin()) {
                $pivot = LogProductClicksUser::where(['user_id' => Auth::user()->id ,
                    'log_product_click_id' => $logProductClick->id])->orderBy('id','DESC')->first();

                if($pivot == null ){
                    Auth::user()->logProductClicks()->attach($logProductClick);
                }else{
                    $date = Carbon::parse($pivot->created_at);
                    $date->addDay();
                    if(Carbon::now() >= $date){
                        //logger("creo nuovo");
                        Auth::user()->logProductClicks()->attach($logProductClick);
                    }else{
                        //logger("ultime 24h");
                    }
                }
            }
        }
    }


    public static function loggerOpenProduct($asin){

        $data = ['new' => false, 'code' => ''];
        $logProductClick = LoggerUser::createOrFindLogProductClick($asin);

        if($logProductClick != null) {
            if (Auth::user() && !Auth::user()->isAdmin()) {

                $idProductClick = LogProductClick::where('asin',$asin)->first();
                if($idProductClick) {
                    $pivot = LogProductClicksUser::where(['user_id' => Auth::user()->id, 'log_product_click_id' => $idProductClick->id])->orderBy('id', 'DESC')->first();
                    //logger($pivot);

                    //dovrebbe esistere sempre
                    if ($pivot == null) {
                        //logger("NON TROVATO");
                        Auth::user()->logProductClicks()->attach($logProductClick);
                        $pivot = LogProductClicksUser::where(['user_id' => Auth::user()->id, 'log_product_click_id' => $idProductClick->id])->orderBy('id', 'DESC')->first();
                    }

                    //cerchiamo se esiste il pivot
                    if ($pivot != null) {
                        if (!$pivot->open && $pivot->code == null) {
                            logger($pivot->id);
                            $id = $pivot->id;
                            $code = Helper::generateBarcodeNumber();
                            Auth::user()->logProductClicks()->newPivotStatement()->where('id', $id)
                                ->update([
                                    'open' => true,
                                    'code' => $code
                                ]);
                            $data['new'] = true;
                            $data['code'] = $code;
                        } else {
                            $data['new'] = false;
                            $data['code'] = $pivot->code;
                        }
                    }
                }
            }
        }

        return $data;
    }

}
