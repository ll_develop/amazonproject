<?php

namespace App\Http\Controllers;

use App\Category;
use App\Model\CacheCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller{

    public function getCategory(Request $request,$nodeid){
        if($nodeid && strlen($nodeid) <= 20) {
            $productsLastClicked = HelperSearch::getProductsLastClicked(12);
            $productsBestSellers = HelperSearch::getProductsBestSellers(12);
            $output = HelperSearch::getCategoryFromNodeId($nodeid);
            //dd($output['response']);
            $data = [
                'errorSearch' => false,
                'errorOutput' => "",
                'categoryName' => "",
                'TopSellers' => "",
                'MostGifted' => "",
                'lastClicked' => $productsLastClicked->all(),
                'bestSellers' => $productsBestSellers->all(),
            ];

            if(!$output['error']){
                $data['categoryName'] = $output['response']['categoryName'];
                //logger($output['response']['TopSellers']);
                $data['TopSellers'] =  $output['response']['TopSellers'];
                $data['MostGifted'] =  $output['response']['MostGifted'];

                //SEGMENT
                if (Auth::user() && !Auth::user()->isAdmin()) {
                    if(!$output['error']) {
                        SegmentController::segmentTrackCategory(
                            Auth::user()->id,
                            SegmentController::EVENT_CATEGORY_CLICK,
                            $nodeid,
                            $data['categoryName']
                        );
                    }
                }

                return view('pages.category',$data);
            }else{
                $data['errorSearch'] =  true;
                $data['errorOutput'] =  $output['response'];
                return view('pages.category', $data);
            }

        }

        return redirect()->route('home');
    }



    public function bestSellers(Request $request){

            $productsLastClicked = HelperSearch::getProductsLastClicked(12);
            $productsBestSellers = HelperSearch::getProductsBestSellers(12);

            $cat = CacheCategory::where('featured',true)->get();
            $catView = CacheCategory::where('name','like','%Elettronica%')->first();

            $output = HelperSearch::getCategoryFromNodeId($catView->nodeid);
//logger($cat);
//dd($output['response']);
            $data = [
                'errorSearch' => false,
                'errorOutput' => "",
                'categoryName' => "",
                'categories' => $cat,
                'TopSellers' => "",
                'MostGifted' => "",
                'lastClicked' => $productsLastClicked->all(),
                'bestSellers' => $productsBestSellers->all(),
            ];

            if(!$output['error']){
                $data['categoryName'] = $output['response']['categoryName'];
                $data['TopSellers'] =  $output['response']['TopSellers'];
                $data['MostGifted'] =  $output['response']['MostGifted'];
                return view('pages.bestsellers',$data);
            }else{
                $data['errorSearch'] =  true;
                $data['errorOutput'] =  $output['response'];
                return view('pages.bestsellers', $data);
            }

            return redirect()->route('home');
    }


}
