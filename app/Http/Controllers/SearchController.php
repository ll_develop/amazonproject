<?php

namespace App\Http\Controllers;

use App\Model\LogSearch;
use App\Model\ProductAmazon;
use App\Model\Search;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;

class SearchController extends Controller{

    private $file_mock = "textsearch.json";

    public function search(Request $request){
        //logger('GET:request->has(search_product) : ' . $request->has('page'));
        $pages = $request->session()->get('search:pages');
        $searchString = $request->session()->get('search:string');

        $productsLastClicked = HelperSearch::getProductsLastClicked(12);
        $productsBestSellers = HelperSearch::getProductsBestSellers(12);

        if($request->has('page') && ctype_digit($request->page) && $pages && $searchString){
            $page  = $request->page;
            $cleanSearch = "_" . Helper::clean($searchString);

            //controllo che page non sia maggiore del numero di pagine disponibili
            if($page > $pages['totalMaxPages']){
                $page = $pages['totalMaxPages'];
            }

            if ($page == 1 && Cache::has($cleanSearch) && env('CACHE_ENABLED')) {
                $response = Cache::get($cleanSearch);
            } else {
                if(env('MOCK_ENABLED')){
                    $response = json_decode(file_get_contents($this->file_mock), true);
                }else{
                    $response = AmazonProduct::search('All', $searchString, $page);
                }
            }

            if (Helper::hasAmazonErrorItems($response)) {
                $data = [
                    'errorSearch' => true,
                    'errorOutput' => Helper::amazonErrorItems($response),
                    'lastClicked' => $productsLastClicked->all(),
                    'bestSellers' => $productsBestSellers->all(),
                ];
                return view('pages.search_product', $data);
            }

            $collection = Helper::convertCollectionAmazonJsonToObject($response);

            if(sizeof($collection) > 0){
                $pages = Helper::getAmazonTotalPages($response,$page);
                $request->session()->put('search:pages', $pages);

                $data = [
                    'errorSearch' => false,
                    'errorOutput' => "",
                    'records' => $collection->all(),
                    'pages' => $pages,
                    'lastClicked' => $productsLastClicked->all(),
                    'bestSellers' => $productsBestSellers->all(),
                ];
                return view('pages.search_product',$data);
            }

        }

        return redirect()->route('home');
    }


    public function searchByString(Request $request){
        //logger('request->has(search_product) : ' . $request->has('search_product'));
        //logger('request->search_product : ' . $request->search_product);

        if ($request->has('search_product') && $request->search_product != '') {
            $searchString = $request->search_product;
            $request->session()->put('search:string', $searchString);

            $productsLastClicked = HelperSearch::getProductsLastClicked(12);
            $productsBestSellers = HelperSearch::getProductsBestSellers(12);

            //Cache Key
            $cleanSearch = "_" . Helper::clean($searchString);

            if (Cache::has($cleanSearch) && env('CACHE_ENABLED')) {
                $response = Cache::get($cleanSearch);
            } else {
                if(env('MOCK_ENABLED')){
                    $response = json_decode(file_get_contents($this->file_mock), true);
                }else{
                    $response = AmazonProduct::search('All', $searchString, 1);
                }
                $expiresAt = now()->addDays(15);
                Cache::add($cleanSearch, $response, $expiresAt);
            }

            if (Helper::hasAmazonErrorItems($response)) {
                $data = [
                    'errorSearch' => true,
                    'errorOutput' => Helper::amazonErrorItems($response),
                     'lastClicked' => $productsLastClicked->all(),
                     'bestSellers' => $productsBestSellers->all(),
                ];
                return view('pages.search_product', $data);
            }

            //dd($response);
            //return response()->json($response);


            $collection = Helper::convertCollectionAmazonJsonToObject($response);

            if(sizeof($collection) > 0){
                LoggerUser::loggerSearch($searchString);

                if (Auth::user() && !Auth::user()->isAdmin()) {
                    SegmentController::segmentTrack(
                        Auth::user()->id,
                        SegmentController::EVENT_SEARCH,
                        [
                            'string' => $searchString,
                            'id_string' => Helper::clean($searchString),
                        ]
                    );
                }

                $pages = Helper::getAmazonTotalPages($response,1);
                $request->session()->put('search:pages', $pages);

                $data = [
                    'errorSearch' => false,
                    'errorOutput' => "",
                    'records' => $collection->all(),
                    'pages' => $pages,
                    'lastClicked' => $productsLastClicked->all(),
                    'bestSellers' => $productsBestSellers->all(),
                ];
                return view('pages.search_product',$data);
            }

            $data = [
                'errorSearch' => true,
                'errorOutput' => "",
                'lastClicked' => $productsLastClicked->all(),
                'bestSellers' => $productsBestSellers->all(),
            ];
            return view('pages.search_product',$data);
        }
        return redirect()->route('home');
    }
}
