<?php

namespace App\Http\Controllers;

use ApaiIO\Operations\Search;
use ApaiIO\Operations\SimilarityLookup;
use App\Model\MyCollection;
use App\Model\ProductAmazon;
use App\Model\TypedList;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;
use SimpleXMLElement;

class TestController extends Controller
{
    //
    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function test2(Request $request)
    {
        $stringToSearch = $request->search;

        if ($stringToSearch != '') {
            $expiresAt = now()->addDays(15);

            $cleanSearch = "_" . $this->clean($stringToSearch);

            if (Cache::has($cleanSearch)) {
                $response = Cache::get($cleanSearch);
            } else {
                $response = AmazonProduct::search('All', $stringToSearch, 2);

                if(Helper::hasAmazonErrorItems($response)){
                    return response()->json(Helper::amazonErrorItems($response));
                }else{
                    Cache::add($cleanSearch, $response, $expiresAt);//TODO questo dev'essere inteligente capire la risposta se è valida o meno
                }

            }

            return response()->json($response);
        }
        return response()->json("Error");
    }

    public function test()
    {
        //try {

            //TODO questa è una ricerca di prodotto mockata
            //$response = json_decode(file_get_contents("textsearch.json"),true);


            //return response()->json($response);
/*
            //$response = AmazonProduct::search('All', 'cuffia bluetooth', 2);
            # returns normal array
            # string $browse Browse node
            //$response = AmazonProduct::browse('473365031');
            if(array_key_exists('Items',$response)) {
                $items = array_get($response, 'Items');
                if(array_key_exists('Item',$items)) {
                    $item = array_get($items, 'Item');

                    $collection = collect();

                    if (sizeof($item) > 0) {
                        foreach ($item as $key => $product) {
                            $productAmazon = new ProductAmazon($product['ASIN']);
                            $productAmazon->setName('NOME_' . $key);

                            $productAmazon->DetailPageURL = $product['DetailPageURL'];
                            $productAmazon->AddToWishListDescrition = $product['ItemLinks']['ItemLink'][0]['Description'];
                            $productAmazon->AddToWishListURL= $product['ItemLinks']['ItemLink'][0]['URL'];
                            $productAmazon->SalesRank= $product['SalesRank'];
                            $productAmazon->LargeImage= $product['LargeImage']['URL'];

                            $collection->push($productAmazon);
                        }
                    } else {

                    }

                    //return response()->json($collection->all());
                    return view('pages.test_page',['records' => $collection->all()]);
                }
            }
        } catch(\Exception $exception) {
            dd($exception);
        }
        */

        # sort by TopSeller

        # Response Group: NewReleases
        //$response = AmazonProduct::browse('473365031','BrowseNodeInfo,MostGifted,NewReleases,MostWishedFor,TopSellers');

        # string $asin ASIN
        /*
        try {
            $response = AmazonProduct::item('B01DLMD5O6');
            //dd($response);
            return response()
                ->json($response);
        } catch(\Exception $exception) {
            dd($exception);
        }
        */

        //$response = AmazonProduct::item('B01DLMD5O6');
        # array $asin ASIN
        //$response = AmazonProduct::items(['ASIN1', 'ASIN2']);
        # setIdType: support only item() and items()
        //$response = AmazonProduct::setIdType('EAN')->item('EAN');
        # reset to ASIN
        //AmazonProduct::setIdType('ASIN');
        //dd(User::where('id',10)->get());
/*
        $results = "nothing";
        try {
            $response = AmazonProduct::browse('412609031','BrowseNodeInfo,TopSellers,MostGifted');
            //$nodes = array_get($response, 'BrowseNodes');
            //$items = array_get($nodes, 'BrowseNode.TopSellers.TopSeller');
            //$items = array_slice($items, 0, 1, true);
            //$asins = array_pluck($items, 'ASIN');
            //dd(array_slice($items, 0, 1, true));
            //$results = AmazonProduct::items($asins);
        } catch(\Exception $exception) {
            //dd($exception);
        }

*/
        $user = User::find(1)->get();
        event(new Registered($user));

        return response()
            ->json("Ok");


        //SEARCH-----------
        /*
        $search = new Search();
        $search->setCategory('All');
        $search->setKeywords('amazon');
        $search->setResponseGroup(['Large']);
        $response = AmazonProduct::run($search);
        */
        //dd($response);
        //SEARCH-----------



        //SIMILARITY B06XCJHGP3
        /*
        $similarity = new SimilarityLookup();
        $similarity->setItemId('B06XCJHGP3');
        $similarity->setResponseGroup(['Medium']);

        return response()
            ->json(AmazonProduct::run($similarity));
        */


        //$category = Category::with('products')->get();
        //$product = Product::find(1);

        /*
       $products  =  Product::with(['categories' => function ($query) {
                $query->where('id', 'like', 1);
       }])->get();
        */
        //$product->restore();
        //aggiungi una categoria al prodotto
        //$product->categories()->attach($category);
        //elimina una categoria dal prodotto
        //$product->categories()->detach($category);

        //aggiungi più prodotti ad una categoria
        //$category = Category::all()->first();
        //$products = Product::where( ... )->lists('id');
        //$category->products()->sync($products);

    }
}
