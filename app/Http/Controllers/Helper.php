<?php
/**
 * Created by IntelliJ IDEA.
 * User: LeandroDev
 * Date: 24/11/2018
 * Time: 17.10
 */

namespace App\Http\Controllers;


use App\Model\LogProductClicksUser;
use App\Model\ProductAmazon;
use Carbon\Carbon;
use DateInterval;
use DateTimeInterface;
use Mockery\Exception;

class Helper
{

    const AMAZON_ERRORS = 'Errors';
    const AMAZON_CODE = 'Code';
    const AMAZON_MESSAGE = 'Message';

    public static function hasAmazonErrorItems($response){
        return Helper::hasAmazonError($response,'Items');
    }

    public static function hasAmazonErrorBrowserNode($response){
        return Helper::hasAmazonError($response,'BrowseNodes');
    }

    public static function hasAmazonError($response,$source){
        $reqAmazon = array_get($response, $source . '.Request');
        return array_key_exists(Helper::AMAZON_ERRORS, $reqAmazon);
    }

    public static function amazonErrorBrowserNode($response){
        return Helper::amazonError($response,'BrowserNodes');
    }

    public static function amazonErrorItems($response){
        return Helper::amazonError($response,'Items');
    }

    public static function amazonError($response,$source){
        $reqAmazon = array_get($response, $source.'.Request');
        $errors = $reqAmazon[Helper::AMAZON_ERRORS];
        $output = "";
        foreach ($errors as $e) {
            if (array_key_exists(Helper::AMAZON_CODE, $e) && array_key_exists(Helper::AMAZON_MESSAGE, $e)){
                if(env('APP_DEBUG')){
                    $output = 'Code: ' . $e[Helper::AMAZON_CODE] . "<br>Message:" . $e[Helper::AMAZON_MESSAGE] . "\n";
                }else{
                    $output = "Message:" . $e[Helper::AMAZON_MESSAGE] . "\n";
                }
            }
        }
        return $output;
    }

    public static function convertAmazonJsonToObject($response){
        if (array_key_exists('Items', $response)) {
            $items = array_get($response, 'Items');
            if (array_key_exists('Item', $items)) {
                $product = array_get($items, 'Item');
                if (array_key_exists('ASIN', $product)) {
                    $productAmazon = self::createProductAmazon($product);
                    return $productAmazon;
                }//asin check
            }
        }
        return null;
    }

    public static function convertAmazonJsonBrowseNodeLookUpToObject($response){
        $collection = collect();
        $collection->put("categoryName" ,null);
        $collection->put("TopSellers" ,null);
        $collection->put("MostGifted" ,null);

        if (array_key_exists('BrowseNodes', $response)) {
            $BrowseNodes = array_get($response, 'BrowseNodes');
            if (array_key_exists('BrowseNode', $BrowseNodes)) {
                $BrowseNode = array_get($BrowseNodes, 'BrowseNode');
                if (array_key_exists('Name', $BrowseNode)) {
                    $Name = array_get($BrowseNode, 'Name');
                    $collection->put("categoryName" ,$Name);

                    if (array_key_exists('TopItemSet', $BrowseNode)) {
                        $TopItemSet= array_get($BrowseNode, 'TopItemSet');
                        if (sizeof($TopItemSet) > 0) {
                            foreach ($TopItemSet as $key => $item) {
                                if ($item['Type'] == "TopSellers"){
                                    $topSellersCollection = collect();
                                    foreach ($item['TopItem'] as $key2 => $it) {
                                        $output = HelperSearch::getProductItem($it['ASIN']);
                                        if(!$output['error']){
                                            $topSellersCollection->push($output['response']);
                                        }
                                    }
                                    $collection->put("TopSellers" ,$topSellersCollection);
                                }else if($item['Type'] == "MostGifted"){
                                    $mostGiftedCollection = collect();
                                    foreach ($item['TopItem'] as $key2 => $it) {
                                        $output = HelperSearch::getProductItem($it['ASIN']);
                                        if(!$output['error']){
                                            $mostGiftedCollection->push($output['response']);
                                        }
                                    }
                                    $collection->put("MostGifted" ,$mostGiftedCollection);
                                }
                            }
                        }
                    }


                    return $collection;
                }//asin check
            }
        }
        return null;
    }

    public static function convertCollectionAmazonJsonToObject($response){
        $collection = collect();
        //Creo OutPut Uscita
        if (array_key_exists('Items', $response)) {
            $items = array_get($response, 'Items');
            if (array_key_exists('Item', $items)) {
                $item = array_get($items, 'Item');
                if (sizeof($item) > 0) {
                    foreach ($item as $key => $product) {
                        if (array_key_exists('ASIN', $product)) {
                            $productAmazon = self::createProductAmazon($product);
                            //---------------------------------------------------------------------------------
                            $collection->push($productAmazon);
                        }//asin check
                    }//foreach
                } else {

                }
            }
        }//key exist
        return $collection;
    }

    public static function getAmazonTotalPages($response,$currentPages){
        $limit = 10;

        $pages = array(
            'totalresults' => 0,
            'totalpages' => 0,
            'totalMaxPages' => 0,
            'currentPages' => $currentPages
        );

        //Creo OutPut Uscita
        if (array_key_exists('Items', $response)) {
            $items = array_get($response, 'Items');
            if (array_key_exists('TotalResults', $items)) {
                $TotalResults = array_get($items, 'TotalResults');
                $pages['totalresults'] = $TotalResults;
            }
            if (array_key_exists('TotalPages', $items)) {
                $TotalPages = array_get($items, 'TotalPages');
                $pages['totalMaxPages'] = $TotalPages;
                if($TotalPages >= $limit)
                    $TotalPages = $limit;
                $pages['totalpages'] = $TotalPages;
            }
        }
        return $pages;
    }

    public static function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public static function createProductAmazon($product){
        if (array_key_exists('ASIN', $product)) {
            $productAmazon = new ProductAmazon($product['ASIN']);

            if (array_key_exists('SalesRank', $product)) {
                $productAmazon->SalesRank = $product['SalesRank'];
            }

            if (array_key_exists('DetailPageURL', $product)) {
                $productAmazon->DetailPageURL = $product['DetailPageURL'];
            }

            if (array_key_exists('ItemAttributes', $product)) {
                $ItemAttributes = array_get($product, 'ItemAttributes');
                if (array_key_exists('Title', $ItemAttributes))
                    $productAmazon->setName($ItemAttributes['Title']);
                if (array_key_exists('Brand', $ItemAttributes))
                    $productAmazon->Brand = $ItemAttributes['Brand'];
                if (array_key_exists('Feature', $ItemAttributes))
                    $productAmazon->Feature = $ItemAttributes['Feature'];
            }

            $productAmazon->Categorias = "";
            if (array_key_exists('BrowseNodes', $product)) {
                $BrowseNodes = array_get($product, 'BrowseNodes');
                if (array_key_exists('BrowseNode', $BrowseNodes)) {
                    $BrowseNode = array_get($BrowseNodes, 'BrowseNode');
                    if (array_key_exists('Name', $BrowseNode)) {
                        $productAmazon->Categorias = $BrowseNode['Name'];
                        $productAmazon->CategoriasId = $BrowseNode['BrowseNodeId'];
                    } else {
                        if (count($BrowseNode) >= 1) {
                            $productAmazon->Categorias = $BrowseNode[0]['Name'];
                            $productAmazon->CategoriasId = $BrowseNode[0]['BrowseNodeId'];
                        }
                    }
                }
            }

            $productAmazon->EditorialReview = "";
            if (array_key_exists('EditorialReviews', $product)) {
                $EditorialReviews = array_get($product, 'EditorialReviews');
                if (array_key_exists('EditorialReview', $EditorialReviews)) {
                    $EditorialReview = array_get($EditorialReviews, 'EditorialReview');
                    if (array_key_exists('Content', $EditorialReview)) {
                        $productAmazon->EditorialReview = $EditorialReview['Content'];
                    } else {
                        if (count($EditorialReview) >= 1) {
                            $productAmazon->Categorias = $EditorialReview[0]['Content'];
                        }
                    }
                }
            }

            //IMAGESET
            $productAmazon->ImageSets = "";
            if (array_key_exists('ImageSets', $product)) {
                $ImageSets = array_get($product, 'ImageSets');
                if (array_key_exists('ImageSet', $ImageSets)) {
                    $productAmazon->ImageSets = $ImageSets['ImageSet'];
                }
            }

            if (array_key_exists('ItemLinks', $product)) {
                $ItemLinks = array_get($product, 'ItemLinks');
                if (array_key_exists('ItemLink', $ItemLinks)) {
                    $ItemLink = array_get($ItemLinks, 'ItemLink');
                    if (count($ItemLink) >= 1) {
                        if (array_key_exists('Description', $ItemLink[0]))
                            $productAmazon->AddToWishListDescrition = $ItemLink[0]['Description'];
                        if (array_key_exists('URL', $ItemLink[0]))
                            $productAmazon->AddToWishListURL = $ItemLink[0]['URL'];
                    }
                    if (count($ItemLink) >= 3) {
                        if (array_key_exists('Description', $ItemLink[2]))
                            $productAmazon->AllCustomerReviewsDescrition = $ItemLink[2]['Description'];
                        if (array_key_exists('URL', $ItemLink[2]))
                            $productAmazon->AllCustomerReviewsURL = $ItemLink[2]['URL'];
                    }
                }
            }

            //TODO check all campi
            if (array_key_exists('SimilarProducts', $product)) {
                $SimilarProducts = array_get($product, 'SimilarProducts');
                if (array_key_exists('SimilarProduct', $SimilarProducts)) {
                    $productAmazon->SimilarProducts = $SimilarProducts['SimilarProduct'];
                }
            }

            $productAmazon->LargeImage = asset('images/no_img/1.jpg');
            if (array_key_exists('LargeImage', $product)) {
                $LargeImage = array_get($product, 'LargeImage');
                if (array_key_exists('URL', $LargeImage)) {
                    $productAmazon->LargeImage = $LargeImage['URL'];
                }
            }

            if (array_key_exists('OfferSummary', $product)) {
                $OfferSummary = array_get($product, 'OfferSummary');
                if (array_key_exists('LowestNewPrice', $OfferSummary)) {
                    $LowestNewPrice = array_get($OfferSummary, 'LowestNewPrice');
                    if (array_key_exists('FormattedPrice', $LowestNewPrice)) {
                        $productAmazon->OfferSummaryFormattedPrice = $LowestNewPrice['FormattedPrice'];
                    }

                }
            }

            return $productAmazon;
        }
        return null;
    }

    public static function generateBarcodeNumber() {
        $number = str_random(40); // better than rand()
        // call the same function if the barcode exists already
        if (Helper::barcodeNumberExists($number)) {
            return Helper::generateBarcodeNumber();
        }
        // otherwise, it's valid and can be used
        return $number;
    }

    private static function barcodeNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return LogProductClicksUser::whereOpen($number)->exists();
    }

    public static function getMinutes($duration){
        $duration = Helper::parseDateInterval($duration);
        if ($duration instanceof DateTimeInterface) {
            $duration = Carbon::now()->diffInSeconds(Carbon::createFromTimestamp($duration->getTimestamp()), false) / 60;
        }
        return (int) ($duration * 60) > 0 ? $duration : null;
    }

    public static function parseDateInterval($delay){
        if ($delay instanceof DateInterval) {
            $delay = Carbon::now()->add($delay);
        }
        return $delay;
    }
}
