<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Helper;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;
use App\Http\Controllers\Controller;

class AmazonProductController extends Controller
{
    private $file_mock = "textitem.json";


    public function add(Request $request){
        return view('admin.add_product');
    }


    public function productByAsin(Request $request){

        $rules = array(
            'asin'=>'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->route('add_product')
                ->withErrors($validator)
                ->withInput();
        }

        $asin = $request->asin;

        try {

            if(env('MOCK_ENABLED')){
                $response = json_decode(file_get_contents($this->file_mock), true);
            }else{
                $response = AmazonProduct::item($asin);
            }

            $request->session()->put('product_to_add:response', $response);
            $request->session()->put('product_to_add:asin', $asin);

            if(Helper::hasAmazonErrorItems($response)){
                $output = Helper::amazonErrorItems($response);
                return redirect()->route('add_product')->with('error', $output);
            }else{
                $item = array_get($response, 'Items.Item');
                //return response()->json($item);
                return view('admin.add_product',['item' => $item]);
            }
            //dd($response);
        } catch(\Exception $exception) {
            dd($exception);
        }

        return redirect()->route('add_product')->with('success', 'Product Found !!!');
    }


    public function addToMyDb(Request $request){

        $response = $request->session()->pull('product_to_add:response');
        $asin = $request->session()->pull('product_to_add:asin');

        if($response == null)
            return redirect()->route('add_product');

        $productAmazon = Helper::convertAmazonJsonToObject($response);


        //cerco il prodotto nel mio db
        $product  =  Product::with('categories')->where('asin',$productAmazon->getAsin())->first();

        if(!$product) {
            $product = new Product();
        }else{
            //recupero le categorie e le aggiorno
            $categories = $product->categories;
            foreach ($categories as $key => $cat) {
                $product->categories()->detach($cat);
            }
        }

        $product->name = $productAmazon->getName();
        $product->asin = $productAmazon->getAsin();
        $product->price = $productAmazon->OfferSummaryFormattedPrice;
        $product->order = 0;

        $category = Category::where('node_id',$productAmazon->CategoriasId)->first();

        if(!$category) {
            //la creo la cateogira
            $category = new Category();
            $category->name = $productAmazon->Categorias;
            $category->node_id = $productAmazon->CategoriasId;
            $category->save();
        }

        $product->payload = \GuzzleHttp\json_encode($response);
        $product->save();
        $product->categories()->attach($category);

        return redirect()->route('add_product')->with('success', 'Product Found !!!');


    }




    public function categoryByNode(Request $request){

        $rules = array(
            'name'=>'required',
            'node'=>'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->route('add_product')
                ->withErrors($validator)
                ->withInput();
        }

            $node = $request->node;
            $name = $request->name;
            $root = $request->root ? true : false;

            $category = new Category();
            $category->name = $name;
            $category->node_id = $node;
            $category->is_category_root = $root;

            $category->save();

            return redirect()->route('add_product')->with('success', 'Category Insert Ok!!!');




    }
}
