<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Helper;
use App\Model\Category;
use App\Model\Contest;
use App\Model\Product;
use App\Model\Winner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;
use App\Http\Controllers\Controller;

class AmazonWinnerController extends Controller
{

    public function createWinner(Request $request){

        try {
            $user_id = 4;
            $contest_id = 1;
            $winner = Winner::where(['user_id' =>$user_id , 'contest_id' => $contest_id])->get()->first();
            if($winner == null) {
                $winner = new Winner();
                self::newWinner($winner,$user_id,$contest_id);
                $winner->save();
                return response()->json($winner);
            }
            return response()->json("Error Winner Exist!!!");


        } catch(\Exception $exception) {
            return response()->json("Error Winner Creation!!!");
        }
    }

    public function updateWinner(Request $request,$id){
        try {
            $winner = Winner::find($id);
            if(!$winner)
                return response()->json("Error Winner Not Exist!!!");
            self::editWinner($winner);
            $winner->save();

            return response()->json($winner);
        } catch(\Exception $exception) {
            return response()->json("Error Winner Creation!!!");
        }

    }

    public function newWinner($winner,$user_id,$contest_id){
        $winner->user_id = $user_id;
        $winner->contest_id = $contest_id;
        $winner->description = "";
        $winner->note = "";
        $winner->take = false;
    }

    public function editWinner($winner){
        $winner->description = "";
        $winner->note = "";
        $winner->take = false;
    }

    public function createDate($stringDate){
        return Carbon::createFromFormat('Y-m-d H', $stringDate)->toDateTimeString();
    }


}
