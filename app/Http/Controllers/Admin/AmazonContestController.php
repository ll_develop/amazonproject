<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Helper;
use App\Model\Category;
use App\Model\Contest;
use App\Model\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;
use App\Http\Controllers\Controller;

class AmazonContestController extends Controller
{

    public function createContest(Request $request){

        try {
            $name = "ConcorsoDF56KT";
            $contest = Contest::where('name',$name)->get()->first();
            if($contest == null) {
                $contest = new Contest();
                self::newContest($contest,$name);
                $contest->save();
                return response()->json($contest);
            }
            return response()->json("Error Contest Exist!!!");
        } catch(\Exception $exception) {
            return response()->json("Error Contest Creation!!!");
        }
    }

    public function updateContest(Request $request,$id){
        try {
            $contest = Contest::find($id);
            if(!$contest)
                return response()->json("Error Contest Not Exist!!!");

            $name = "contest_1";
            self::newContest($contest,$name);
            $contest->save();

            return response()->json($contest);
        } catch(\Exception $exception) {
            return response()->json("Error Contest Creation!!!");
        }

    }

    public function newContest($contest,$name){
        $contest->name = $name;
        $contest->start = self::createDate('2019-12-15 00');
        $contest->end = self::createDate('2019-01-31 00');
        $contest->extraction = self::createDate('2019-02-03 00');
        $contest->winners = \GuzzleHttp\json_encode(array(0, 0, 0, 0));
        $contest->image = "/contest/150euro.jpg";
        $contest->gift = "150 Euro";
        $contest->description = "Un Buono da 150 Euro";
        $contest->number_of_participants = 10;
        $contest->active = false;
    }

    public function createDate($stringDate){
        return Carbon::createFromFormat('Y-m-d H', $stringDate)->toDateTimeString();
    }


}
