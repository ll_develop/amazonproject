<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class SocialController extends Controller
{
    public function Redirect($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider){
        try{
            $userSocial =   Socialite::driver($provider)->stateless()->user();
            $user       =  User::where(['email' => $userSocial->getEmail()])->first();

            if($user){
                $user->name = $userSocial->getName();
                $user->image = $userSocial->getAvatar();
                $user->provider_id = $userSocial->getId();
                $user->provider = $provider;
                $user->update();
            }else{
                $user = User::create([
                    'name'          => $userSocial->getName(),
                    'email'         => $userSocial->getEmail(),
                    'image'         => $userSocial->getAvatar(),
                    'provider_id'   => $userSocial->getId(),
                    'provider'      => $provider,
                ]);

                event(new Registered($user));
            }

            $remember = true;
            Auth::login($user,$remember);


            if(session()->has('url.intended')) {
                return redirect(session()->get('url.intended'));
            }
            return redirect()->route('home');
        }catch (RequestException $ex){
            abort(501);
        }
    }
}
