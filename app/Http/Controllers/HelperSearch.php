<?php
namespace App\Http\Controllers;

use App\Model\CacheCategory;
use App\Model\LogProductClick;
use App\Model\LogProductClicksUser;
use App\Model\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;
use Illuminate\Support\Facades\Cache;

class HelperSearch{

    const AMAZON_ERRORS = 'Errors';
    const FILE_MOCK = "textsearch.json";

    public static function getProductsOurChoice($n){
        $collection = collect();

        for ($i = 1; $i <= $n; $i++) {
            $product = Product::where('order',$i)->first();
            if($product){
                $response = json_decode($product->payload, true);
                $productAmazon = Helper::convertAmazonJsonToObject($response);
                $collection->push($productAmazon);
            }
        }
        return $collection;
    }

    public static function getProductsLastClicked($n){
        $collection = collect();

        $productClicksAsin = LogProductClick::all()
            ->sortByDesc("created_at")
            ->take($n);

        foreach ($productClicksAsin as $key => $p) {
            $output = HelperSearch::getProductItem($p->asin);
            //logger($p->asin);
            if(!$output['error']){
                $collection->push($output['response']);
            }
        }//foreach

        return $collection;
    }

    public static function getProductsBestSellers($n){
        $collection = collect();

        $productClicksAsin = LogProductClick::with('users')->get();

        $bestSellers = $productClicksAsin->sortByDesc(function ($item, $key) {
            $item->count =  count($item->users) ;
            array_pull($item,'users');
            return $item->count;
        })->values()->take($n);

        foreach ($bestSellers as $key => $p) {
            $output = HelperSearch::getProductItem($p->asin);
            //logger($p->asin);
            if(!$output['error']){
                $collection->push($output['response']);
            }
        }//foreach

        return $collection;
    }

    public static function getProductItem($asin){
        $output = [
            'error' => false,
            'response' => ""
        ];

        $cleanAsin = "_" . Helper::clean($asin);
        //cerco prodotto nel mio DB
        $product = Product::where('asin', $asin)->first();
        //esiste
        if ($product) {
            $response = json_decode($product->payload, true);
        } else {
            if (Cache::has($cleanAsin) && env('CACHE_ENABLED')) {
                $response = Cache::get($cleanAsin);
            } else {
                if (env('MOCK_ENABLED')) {
                    $response = json_decode(file_get_contents(HelperSearch::FILE_MOCK), true);
                } else {
                    $response = AmazonProduct::item($asin);
                }

                if (Helper::hasAmazonErrorItems($response)) {
                    $output['error'] = true;
                    $output['response'] = Helper::amazonErrorItems($response);
                } else {
                    $expiresAt = now()->addDays(15);
                    Cache::add($cleanAsin, $response, $expiresAt);
                }
            }
        }

        if (!$output['error']) {
            $output['error'] = false;
            $output['response'] = Helper::convertAmazonJsonToObject($response);
        }

        return $output;
    }


    public static function getCategoryFromNodeId($nodeid){

        $output = [
            'error' => false,
            'response' => ""
        ];

        //cerco prodotto nel mio DB
        $cat = CacheCategory::where('nodeid', $nodeid)->first();
        //esiste
        $outputResponse = null;

        if ($cat && !$cat->expired) {
            $response = json_decode($cat->payload, true);
            $outputResponse = Helper::convertAmazonJsonBrowseNodeLookUpToObject($response);
        } else {

            try {
                logger("AmazonProduct::browse : " . $nodeid);
                $response = AmazonProduct::browse($nodeid,'BrowseNodeInfo,TopSellers,MostGifted');

                if (Helper::hasAmazonErrorBrowserNode($response)) {
                        $output['error'] = true;
                        $output['response'] = Helper::amazonErrorBrowserNode($response);
                } else {
                        if(!$cat){
                            $cat = new CacheCategory();
                        }
                        $minutes = now()->addDays(15);
                        $expiration = Carbon::now()->getTimestamp() + (int) (Helper::getMinutes($minutes) * 60);
                        $cat->expiration = $expiration;
                        $cat->nodeid = $nodeid;
                        $cat->payload = \GuzzleHttp\json_encode($response);

                        $outputResponse = Helper::convertAmazonJsonBrowseNodeLookUpToObject($response);
                        $cat->name = $outputResponse['categoryName'];
                        $cat->save();
                }
            } catch(\Exception $exception) {
                logger("catch");
                $output['error'] = true;
            }
        }

        if (!$output['error']) {
            $output['error'] = false;
            $output['response'] = $outputResponse;
        }

        return $output;
    }


}
