<?php

namespace App\Http\Controllers;

use App\Model\Product;
use Illuminate\Http\Request;
use App\Model\ProductAmazon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;

class ProductController extends Controller{


    public function getProduct(Request $request,$asin){

        if($asin && strlen($asin) <= 20){
            $productsLastClicked = HelperSearch::getProductsLastClicked(12);
            $productsBestSellers = HelperSearch::getProductsBestSellers(12);


            $output = HelperSearch::getProductItem($asin);

            $data = [
                'errorSearch' => false,
                'errorOutput' => "",
                'productAmazon' => "",
                'lastClicked' => $productsLastClicked->all(),
                'bestSellers' => $productsBestSellers->all(),
            ];

            if(!$output['error']){
                LoggerUser::loggerClickProduct($asin);

                //SEGMENT
                if (Auth::user() && !Auth::user()->isAdmin()) {
                    $product = $output['response'];
                    SegmentController::segmentTrackProduct(
                        Auth::user(),
                        SegmentController::EVENT_PRODUCT_CLICK,
                        $asin,
                        $product->getName(),
                        $product->CategoriasId,
                        $product->Categorias
                    );
                }

                $data['productAmazon'] =  $output['response'];
                return view('pages.product',$data);
            }else{
                $data['errorSearch'] =  true;
                $data['errorOutput'] =  $output['response'];
                return view('pages.product', $data);
            }
        }
        return redirect()->route('home');
    }

    public function loggerOpenProduct(Request $request,$asin){
        logger("loggerOpenProduct");
        if($request->ajax() && Auth::user() && $asin) {
            $data = LoggerUser::loggerOpenProduct($asin);

            //SEGMENT
            if (Auth::user() && !Auth::user()->isAdmin()) {
                $output = HelperSearch::getProductItem($asin);
                if(!$output['error']) {
                    $product = $output['response'];
                    SegmentController::segmentTrackProduct(
                        Auth::user(),
                        SegmentController::EVENT_PRODUCT_OPEN,
                        $asin,
                        $product->getName(),
                        $product->CategoriasId,
                        $product->Categorias
                    );
                }
            }
            //logger($data);
            return response()->json($data);
        }
    }
}
