<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Segment;

class SegmentController {

    //PAGES
    const PAGE_CAT_DOC = 'Documentation';
    const PAGE_CAT_SRC = 'Search';
    const PAGE_CAT_CONTACT = 'Contact';

    const PAGE_CAT_NAME_RULES = 'Rules Contest';
    const PAGE_CAT_NAME_CONTEST = 'Winners e Contest';
    const PAGE_CAT_NAME_SEARCH_ASIN = 'Search By Asin';
    const PAGE_CAT_NAME_CONTACT = 'Contact Form';

    //TRACK
    const EVENT_PRODUCT_CLICK = 'Product Click';
    const EVENT_PRODUCT_OPEN = 'Product Open';
    const EVENT_CATEGORY_CLICK = 'Category Click';
    const EVENT_SEARCH = 'Search Product';

    public static function segmentIdentify($user){
        if(env('SEGMENT')) {
            logger('segmentIdentify');
            logger($user->name);
            try {
                Segment::identify(array(
                    "userId" => $user->id,
                    "traits" => array(
                        "name" => $user->name,
                        "email" => $user->email
                    )
                ));
            } catch (Exception $ex) {

            }
        }
    }

    public static function segmentPage($userId,$category,$name,$url){
        if(env('SEGMENT')) {
            logger('segmentTrack');
            try {
                Segment::page(array(
                    "userId" => $userId,
                    "category" => $category,
                    "name" => $name,
                    "properties" => array(
                        "url" => $url
                    )
                ));
            } catch (Exception $ex) {

            }
        }
    }

    public static function segmentTrackProduct($user,$event,$productId,$productName,$categoryId,$categoryName){
        if(env('SEGMENT')) {
            logger('segmentTrack');
            try {
                Segment::track([
                    "userId" => $user->id,
                    "email" => $user->email,
                    "event" => $event,
                    "properties" => [
                        "productId" => $productId,
                        "productName" => $productName,
                        "categoryName" => $categoryName,
                        "categoryId" => $categoryId,
                    ]
                ]);
            } catch (Exception $ex) {
            }
        }
    }

    public static function segmentTrackCategory($userId,$event,$categoryId,$categoryName){
        if(env('SEGMENT')) {
            logger('segmentTrack');
            try{
                Segment::track([
                    "userId"     => $userId,
                    "event"      => $event,
                    "properties" => [
                        "categoryId" => $categoryId,
                        "categoryName" => $categoryName,
                    ]
                ]);
            }catch (Exception $ex){
            }
        }
    }

    public static function segmentTrack($userId,$event,$array){
        if(env('SEGMENT')) {
            logger('segmentTrack');
            try {
                Segment::track([
                    "userId" => $userId,
                    "event" => $event,
                    "properties" => $array
                ]);
            } catch (Exception $ex) {

            }
        }
    }

}
