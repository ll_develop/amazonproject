<?php

namespace App\Http\Controllers;

use App\Model\Contest;
use App\Model\LogProductClicksUser;
use Illuminate\Http\Request;
use App\Jobs\SendEmailJob;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{

    public function rules(Request $request){
        $data = [];
        if(Auth::user()) {
            SegmentController::segmentPage(Auth::user()->id,
                SegmentController::PAGE_CAT_DOC,
                SegmentController::PAGE_CAT_NAME_RULES,
                url()->full()
            );
        }
        return view('pages.rules',$data);
    }

    public function contests(Request $request){
        $codeGenerate = 0;
        $contestActive = Contest::where('active',true)->first();
        if($contestActive){
            $myClickCode = LogProductClicksUser::whereBetween('updated_at', [$contestActive->start, $contestActive->end])->get();
        }else{
            $contestActive = Contest::all()->last();
            $myClickCode = LogProductClicksUser::whereBetween('updated_at', [$contestActive->end, now()])->get();
        }

        if($myClickCode){
            $codeGenerate = $myClickCode->count();
        }


        $data = [
            'contestActive' => $contestActive,
            'codeGenerate' => $codeGenerate,
        ];

        if(Auth::user()) {
            SegmentController::segmentPage(Auth::user()->id,
                SegmentController::PAGE_CAT_DOC,
                SegmentController::PAGE_CAT_NAME_CONTEST,
                url()->full()
            );
        }
        return view('pages.contests',$data);
    }

    public function searchByAsin(Request $request){
        $data = [];
        if(Auth::user()) {
            SegmentController::segmentPage(Auth::user()->id,
                SegmentController::PAGE_CAT_SRC,
                SegmentController::PAGE_CAT_NAME_SEARCH_ASIN,
                url()->full()
            );
        }
        return view('pages.search_by_asin',$data);
    }

    public function searchByAsinPost(Request $request){
        $rules = array(
            'asin'=>'required|max:20',
        );

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return redirect()->route('search_by_asin')
                ->withErrors($validator)
                ->withInput();
        }

        $asin = $request->asin;
        $output = HelperSearch::getProductItem($asin);

        if($output['error']){
            return redirect()->route('search_by_asin')->with('error', trans('msg.search.error_empty.f1b'));
        }

        return redirect()->route('get_product', $asin);
    }



    public function shop(Request $request){
        $data = [];
        return view('pages.shop',$data);
    }

    public function contact(Request $request){
        $data = [];
        if(Auth::user()) {
            SegmentController::segmentPage(Auth::user()->id,
                SegmentController::PAGE_CAT_CONTACT,
                SegmentController::PAGE_CAT_NAME_CONTACT,
                url()->full()
            );
        }
        return view('pages.contact',$data);
    }

    public function mailsend(Request $request){

        //Validation
        $rules = [
            'name' => 'required|max:50',
            'email' => 'required|email|max:50',
            'phone' => 'nullable',
            'message' => 'required|max:500'
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
            return redirect('contact')->withInput()->withErrors($validator);
        }

        $data = $request->all();

        $name = $data['name'];
        $email = $data['email'];
        $phone = $data['phone'];
        $msg = $data['message'];

        $data = [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'msg' => $msg,
            'subject' => 'Contact Us',
            'admin_email' => env('MAIL_ADMIN')
        ];


        $emailJob = (new SendEmailJob($data))->delay(Carbon::now()->addMinute(1));
        dispatch($emailJob);

        return redirect()->back()->with('message',  trans('msg.contact.success'));
    }


}
