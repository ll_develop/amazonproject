<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\LogProductClick;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;

//https://www.amazon.it/gp/bestsellers/electronics/?ie=UTF8&ref_=sv_ce_1
class HomeController extends Controller{

    public function index(Request $request){

        $request->session()->pull('search:string');
        //*************************************************************
        $productsOurChoice = HelperSearch::getProductsOurChoice(12);
        $productsLastClicked = HelperSearch::getProductsLastClicked(12);
        $productsBestSellers = HelperSearch::getProductsBestSellers(12);

        $data = [
            'ourChoice' => $productsOurChoice->all(),
            'lastClicked' => $productsLastClicked->all(),
            'bestSellers' => $productsBestSellers->all(),
        ];

        //**************************************************************
        return view('pages.home',$data);
    }

}
