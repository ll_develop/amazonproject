<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            DB::connection()->getPdo();
            return $next($request);
        }catch(Exception $e) {
            return new Response(view('errors.no_connection'));
        }
    }
}
