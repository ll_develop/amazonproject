<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogSearch extends Model
{
    //

    public function users() {
        return $this->belongsToMany('App\User', 'log_searches_users')->withTimestamps();
    }
}
