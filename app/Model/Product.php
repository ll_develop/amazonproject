<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'products';
    protected $fillable = ['categories','updated_at','asin'];

    public function categories() {
        return $this->belongsToMany('App\Model\Category', 'products_categories');
    }
}
