<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogProductClick extends Model
{
    //
    public function users() {
        return $this->belongsToMany('App\User', 'log_product_clicks_users')->withTimestamps();
    }
}
