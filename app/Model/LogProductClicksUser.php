<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogProductClicksUser extends Model{
    //
    public function logProductClicks()
    {
        return $this->hasOne('App\Model\LogProductClick','id','log_product_click_id');
    }
}
