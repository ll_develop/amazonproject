<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CacheCategory extends Model
{
    //

    public function isExpired() {
        return Carbon::now()->getTimestamp() >= $this->expiration;
    }
}
