<?php

namespace App\Model;

class ProductAmazon{

    private $asin;
    private $name;

    public $DetailPageURL;
    public $AddToWishListDescrition;
    public $AllCustomerReviewsDescrition;
    public $AddToWishListURL;
    public $AllCustomerReviewsURL;
    public $SalesRank;
    public $LargeImage;
    public $OfferSummaryFormattedPrice;
    public $Brand;
    public $Feature;
    public $EditorialReview;
    public $SimilarProducts;
    public $Categorias;
    public $CategoriasId;
    public $BrowseNode;
    public $ImageSets;

    public function __construct($asin) {
        $this->asin =  $asin;;
    }


    public function __toString() {
        return $this->asin . "";
    }

    /**
     * @return mixed
     */
    public function getAsin(){
        return $this->asin;
    }

    /**
     * @param mixed $asin
     */
    public function setAsin($asin){
        $this->asin = $asin;
    }

    /**
     * @return mixed
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name){
        $this->name = $name;
    }


}
