<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    //
    public function contest(){
        return $this->hasOne('App\Model\Contest','id','contest_id');
    }
}
