<?php
return [
    'api_key'        => env('AMAZON_API_KEY', ''),
    'api_secret_key' => env('AMAZON_API_SECRET_KEY', ''),
    'associate_tag'  => env('AMAZON_ASSOCIATE_TAG', ''),
    'country'        => env('AMAZON_COUNTRY', 'com'),
];

//https://docs.aws.amazon.com/en_us/AWSECommerceService/latest/DG/Locales.html

//https://github.com/kawax/laravel-amazon-product-api
