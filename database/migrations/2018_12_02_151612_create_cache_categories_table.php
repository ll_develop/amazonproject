<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCacheCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cache_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nodeid')->nullable();
            $table->mediumText('name')->nullable();
            $table->boolean('is_category_root')->default(0);
            $table->boolean('featured')->default(0);
            $table->mediumText('payload');
            $table->integer('expiration');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cache_categories');
    }
}
