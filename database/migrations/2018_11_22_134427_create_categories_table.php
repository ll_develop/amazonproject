<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $t) {
            $t->increments('id');
            // Nested Set related fields
            $t->boolean('is_category_root')->default(0);
            $t->integer('parent_id')->nullable();
            $t->string('name')->nullable();
            $t->string('node_id')->nullable();
            // Indexes
            $t->index('parent_id');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
