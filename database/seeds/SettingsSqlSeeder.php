<?php

use Illuminate\Database\Seeder;

class SettingsSqlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=SettingsSqlSeeder
        $sql = file_get_contents(database_path('seeds/set.sql'));
        DB::unprepared($sql);
        $this->command->info('Setting table imported!');
    }
}
