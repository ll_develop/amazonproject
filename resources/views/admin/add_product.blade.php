@extends('admin.app')
@section('title', 'Home')
@section('content')
<div class="container">
    <h2>Add Product</h2>

    @if(Session::has('success'))
    <div class="alert alert-success ">
            {!! Session::get('success') !!}
    </div>
    @endif

    @if(Session::has('error'))
    <div class="alert alert-danger">
            {!! Session::get('error') !!}
    </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('product_by_asin') }}" id="formID" >
                {{ csrf_field() }}
                <div class="form-group" style="margin-top: 40px;">
                    <div class="effect1 w-size9">
                        <label for="asin" class="control-label">Asin</label>
                        <input class="s-text7 w-full p-b-5" type="text" name="asin" placeholder="B01DLMD5O6" value="B01DLMD5O6">
                        <span class="effect1-line"></span>
                    </div>
                    @if ($errors->has('asin'))
                    <div class="alert alert-danger  w-size9  m-t-15">
                        <ul>
                            @foreach ($errors->get('asin') as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                <div class="w-size2 p-t-20">
                    <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                        Check
                    </button>
                </div>
            </form>

            <form class="form-horizontal m-t-50 m-b-50" role="form" method="POST" action="{{ route('category_by_node') }}" id="formCat" >
                {{ csrf_field() }}
                <h2>Add Category</h2>
                <div class="form-group" style="margin-top: 40px;">
                    <div class="effect1 w-size9">
                        <label for="asin" class="control-label">Node</label>
                        <input class="s-text7 w-full p-b-15" type="text" name="node" placeholder="Node Categoria">
                        @if ($errors->has('node'))
                        <div class="alert alert-danger  w-size9  m-t-15">
                            <ul>
                                @foreach ($errors->get('node') as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    <div class="effect1 w-size9">
                        <label for="asin" class="control-label">Nome</label>
                        <input class="s-text7 w-full p-b-15" type="text" name="name"  placeholder="Nome Categoria">
                        @if ($errors->has('name'))
                        <div class="alert alert-danger  w-size9  m-t-15">
                            <ul>
                                @foreach ($errors->get('name') as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>

                    <div class="">
                        <label for="asin" class="control-label">Root</label>
                        <input type="checkbox" name="root">
                    </div>

                </div>
                <div class="w-size2 p-t-20">
                    <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                        Add CAtegory
                    </button>
                </div>
            </form>
        </div>
        <div class="col-md-8">

            <div class="row">
                <div class="col-md-4">
                    @if(!empty($item))
                        <b>ASIN :</b>{{ @$item['ASIN'] }} <br>
                        <b>SalesRank :</b>    {{ @$item['SalesRank'] }} <br>
                        <hr>
                        <a href="{{ @$item['DetailPageURL'] }}" target="_blank"><b>DetailPageURL</b> </a>  <br>
                    @endif
                </div>
                <div class="col-md-8">
                    <!-- block1 -->
                    @if(!empty($item))
                        <div class="block1 hov-img-zoom pos-relative m-b-30">
                            <img src="{{ @$item['LargeImage']['URL'] }}" width="{{ $item['LargeImage']['Width'] }}" height="{{ $item['LargeImage']['Height'] }}" alt="IMG-BENNER">

                            <div class="block1-wrapbtn w-size2">
                                <a class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4"
                                   onclick="event.preventDefault();
                                                     document.getElementById('addToMyDb').submit();">
                                    Add To DB
                                </a>
                                <form id="addToMyDb" action="{{ route('addToMyDb') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if(!empty($item))
                    @json(@$item['BrowseNodes']) <br>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-50 m-b-50">
        <div class="col-md-4">

        </div>
    </div>
</div>
@endsection
