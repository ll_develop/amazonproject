@extends('app')
@section('title', __('msg.menu_user.dashboard'))
@section('content')

<section class="banner bgwhite p-t-20 p-b-100">
        <div class="container">
            <div class="sec-title p-b-0">
                <h3 class="m-text16 t-center">
                    {{ trans('msg.menu_user.dashboard') }}
                </h3>
            </div>

        <div class="row">
            <div class="col-md-6 col-12 p-t-20">
                <div class="contact_form bo9 card-2 p-t-25 p-l-25  p-r-25  p-b-25">
                    <h3 class="m-text16 m-b-25  t-center">
                        {{ trans('msg.menu_user.dashboard_last_code') }}
                    </h3>



                    @if($productClicked && count($productClicked) > 0)
                        <div class="flex-w flex-sb-m p-b-12">
                            <span class="s-text18 w-size19 w-full-sm">
                                {{ trans('msg.menu_user.dashboard_data') }}:
                            </span>

                            <span class="m-text21 w-size20 w-full-sm">
                                {{ trans('msg.menu_user.dashboard_code') }}
                            </span>
                        </div>

                        @foreach ($productClicked as $key=>$p)
                                <div class="bo10b">
                                    <div class="flex-w flex-sb-m p-b-12">
                                        <span class="s-text18 w-size19 w-full-sm">
                                            {{ \Carbon\Carbon::parse($p->updated_at)->format('d/m/Y H:i')}}
                                        </span>

                                        <span class="s-text16 w-size20 w-full-sm">
                                             {{ $p->code}}
                                            @if($p->logProductClicks && $p->logProductClicks->asin)
                                            <a href="{{ route('get_product',$p->logProductClicks->asin)}}">{{ trans('msg.menu_user.prod_rel') }}</a>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                        @endforeach


                    <div class="wrap-dropdown-content p-t-15 p-b-14 active-dropdown-content">
                        <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4 show-dropdown-content">
                            {{ trans('msg.menu_user.important') }}
                            <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                            <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                        </h5>

                        <div class="dropdown-content dis-none p-t-15 p-b-23" style="display: block;">
                            <p class="s-text8">
                                {{ trans('msg.menu_user.important_des') }}
                               </p>
                        </div>
                    </div>

                    <li class="p-t-4">
                        <div class="s-text13 t-right">
                            <a href="{{ route('user_mycode') }}">
                                <u>{{ trans('msg.menu_user.all_code') }}</u>
                            </a>
                        </div>
                    </li>

                    @else
                        <div class="s-text13 active1">
                            {{ trans('msg.menu_user.dashboard_last_code_empty') }} <a href="{{ route('get_bestSellers') }}"><u>BestSellers</u></a>
                        </div>
                    @endif




                </div>


                <div class="contact_form bo9 card-2 m-t-25 p-t-25 p-l-25  p-r-25  p-b-25">
                    <h3 class="m-text16 m-b-25  t-center">
                        {{ trans('msg.menu_user.dashboard_gift_win') }}
                    </h3>




                        <table style="width: 100%;">
                            <tr>
                                <th style="padding-bottom: 5px;">Date</th>
                                <th>Premio</th>
                                <th>Ritirato</th>
                            </tr>
                    @if($winGift && count($winGift) > 0)
                        @foreach ($winGift as $key=>$g)
                            <tr>
                                <td style="padding-bottom: 5px;">{{ \Carbon\Carbon::parse($g->updated_at)->format('d/m/Y H:i')}}</td>
                                <td>
                                    @if($g->contest && $g->contest->gift)
                                    {{ $g->contest->gift}}
                                    @endif
                                </td>
                                <td>
                                    @if($g->take)
                                        <i class="fs-21 fa fa-check" aria-hidden="true"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </table>
                     @else
                        <tr>
                            <td style="padding-bottom: 5px;">-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        </table>
                        <div class="s-text13 active1">
                            {{ trans('msg.menu_user.dashboard_gift_empty') }} <a href="{{ route('get_bestSellers') }}"><u>BestSellers</u></a>
                        </div>
                     @endif

                </div>
            </div>

            <div class="col-md-6 p-b-0">
                <div class="leftbar p-t-20 p-r-20 p-r-0-sm">

                        <h4 class="m-text14 p-b-1" >
                            @if($contestActive)
                                {{ trans('msg.menu_user.code_generate') }} :
                            @else
                                {{ trans('msg.menu_user.code_generate_off') }} :
                            @endif
                        </h4>
                        <h4 class="m-text14 p-b-1" style="font-size: 44px;">
                            {{ $codeGenerate }}
                        </h4>


                    @if($contestActive)

                        <h4 class="m-text14 p-b-1" >
                            {{ trans('msg.menu_user.expire_at') }} :
                        </h4>

                        <h4 class="m-text14 p-b-1 bo10b" style="font-size: 44px;">
                            <div data-countdown="{{ \Carbon\Carbon::parse($contestActive->end)->format('Y/m/d')}}"></div>
                        </h4>

                        <div class="p-t-10 p-b-5">
                            <h4 class="m-text14 p-t-0 p-b-1 " style="font-size: 21px;color: #7a884b;">
                                {{ trans('msg.menu_user.bonus_to_win') }} : {{ $contestActive->gift }}
                            </h4>
                            <h4 class="m-text14 p-t-0 p-b-1 " style="font-size: 12px;color: #525253;">
                                {{ $contestActive->description }}
                            </h4>
                            <img src="{{ asset($contestActive->image) }}" >
                        </div>


                        <h4 class="s-text21 p-t-0 p-b-7" style="font-size: 11px;color: #424242;">
                            <ul>
                                <li> {{ trans('msg.menu_user.contest_number') }} : {{ $contestActive->name }} </li>
                                <li>
                                    {{ trans('msg.menu_user.start') }}
                                    {{ \Carbon\Carbon::parse($contestActive->start)->format('d/m/Y')}} | {{ trans('msg.menu_user.end') }}
                                    {{ \Carbon\Carbon::parse($contestActive->end)->format('d/m/Y')}}
                                </li>
                                <li> {{ trans('msg.menu_user.exstract_to') }} : {{ \Carbon\Carbon::parse($contestActive->extraction)->format('d/m/Y')}}</li>
                            </ul>
                        </h4>
                    @else
                        <div class="m-text21 active1 p-t-10 p-b-25">
                            {{ trans('msg.menu_user.contest_off') }}
                        </div>
                    @endif
                    <hr>

                    <ul class=" p-t-25 p-b-54">
                        <li class="p-t-4">
                            <div class="s-text13 active1">
                                {{ trans('msg.contact.info_1') }}
                            </div>
                        </li>

                        <li class="p-t-4">
                            <div class="s-text13">
                                {{ trans('msg.contact.mail_1') }}
                            </div>
                        </li>

                        <li class="p-t-4">
                            <div class="s-text13">
                                <hr>
                            </div>
                        </li>

                        <li class="p-t-4">
                            <div  class="s-text13 active1">
                                {{ trans('msg.contact.info_2') }}
                            </div>
                        </li>

                        <li class="p-t-4">
                            <div  class="s-text13">

                                {{ trans('msg.contact.mail_2') }}
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection

@section('script_page')
<script>
    $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
            $this.html(event.strftime('%D {{ trans('msg.menu_user.days') }} %H:%M:%S'));
        });
    });
</script>

@endsection
