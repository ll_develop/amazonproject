@extends('app')
@section('title', __('msg.header.menu.rules'))
@section('content')

<section class="banner bgwhite p-t-20 p-b-100">
        <div class="container">
            <div class="sec-title p-b-0">
                <h3 class="m-text16 t-center">
                    {{ trans('msg.menu_user.all_code') }}
                </h3>
            </div>

        <div class="row">
            <div class="col-md-12 col-12 p-t-20">
                <div class="contact_form bo9 card-2 p-t-25 p-l-25  p-r-25  p-b-25">

                    @if($productClicked && count($productClicked) > 0)
                        <div class="flex-w flex-sb-m p-b-12">
                            <span class="s-text18 w-size19 w-full-sm">
                                {{ trans('msg.menu_user.dashboard_data') }}:
                            </span>

                            <span class="m-text21 w-size20 w-full-sm">
                                {{ trans('msg.menu_user.dashboard_code') }}
                            </span>
                        </div>

                        @foreach ($productClicked as $key=>$p)
                                <div class="bo10b">
                                    <div class="flex-w flex-sb-m p-b-12">
                                        <span class="s-text18 w-size19 w-full-sm">
                                            {{ \Carbon\Carbon::parse($p->updated_at)->format('d/m/Y H:i')}}
                                        </span>

                                        <span class="s-text16 w-size20 w-full-sm">
                                             {{ $p->code}}
                                        </span>

                                        @if($p->logProductClicks && $p->logProductClicks->asin)
                                        <span class="s-text16 w-size20 w-full-sm">
                                            <a href="{{ route('get_product',$p->logProductClicks->asin)}}">{{ trans('msg.menu_user.prod_rel') }}</a>
                                        </span>
                                        @else
                                        <span class="s-text16 w-size20 w-full-sm">
                                            .
                                        </span>
                                        @endif
                                    </div>
                                </div>
                        @endforeach

                    <div class="p-t-25 p-b-25">
                        {{ $productClicked->links() }}
                    </div>


                    <div class="wrap-dropdown-content p-t-15 p-b-14 active-dropdown-content">
                        <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4 show-dropdown-content">
                            {{ trans('msg.menu_user.important') }}
                            <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                            <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                        </h5>

                        <div class="dropdown-content dis-none p-t-15 p-b-23" style="display: block;">
                            <p class="s-text8">
                                {{ trans('msg.menu_user.important_des') }}
                               </p>
                        </div>
                    </div>


                    @else
                        <div class="s-text13 active1">
                            {{ trans('msg.menu_user.dashboard_last_code_empty') }} <a href="{{ route('get_bestSellers') }}"><u>BestSellers</u></a>
                        </div>
                    @endif


                    <li class="p-t-4">
                        <div class="s-text13 t-right">
                            <a href="{{ route('user_dashboard') }}">
                                <u>{{ trans('msg.menu_user.dashboard') }}</u>
                            </a>
                        </div>
                    </li>

                </div>
            </div>

        </div>
    </div>
</section>


@endsection

