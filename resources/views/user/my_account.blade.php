@extends('app')
@section('title', __('msg.menu_user.my_account'))
@section('content')

<section class="bgwhite p-t-20 p-b-60">
    <div class="container">
        <h3 class="m-text16 m-b-1  t-center">
            {{ trans('msg.menu_user.my_account') }}
        </h3>
        <div class="row ">
                <div class="col-md-6 col-12 p-t-20">
                    <div class="contact_form bo9 card-1 p-t-25 p-l-25  p-r-25  p-b-25">
                        <form method="POST" action="{{ route('user_myaccountEdit') }}" enctype="multipart/form-data">
                            <h3 class="m-text12 m-b-25  t-center">
                                {{ trans('msg.menu_user.my_account.info') }}
                            </h3>
                            @if(Session::has('success-info'))
                                <div class="alert alert-success" >
                                    {{ Session::get('success-info') }}
                                </div>
                            @endif

                            {{ csrf_field() }}

                            <label for="name" >{{ __('msg.auth.name') }}</label>
                            <div class="bo4 of-hidden size15 m-b-20">
                                <input type="text" class="sizefull s-text7 p-l-22 p-r-22" id="name" name="name" value="{{ $editprofile->name }}"  placeholder="{{ trans('msg.contact.name') }} *">
                            </div>

                            @if ($errors->has('name'))
                            <div class="alert alert-danger" >
                                @foreach ($errors->get('name') as $error)
                                {{ $error }}<br>
                                @endforeach
                            </div>
                            @endif

                            <label for="name" >{{ __('msg.contact.phone') }}</label>
                            <div class="bo4 of-hidden size15 m-b-20">
                                <input type="text" class="sizefull s-text7 p-l-22 p-r-22 " id="phone" name="phone" value="{{ $editprofile->phone }}" placeholder="{{ trans('msg.contact.phone') }}">
                            </div>

                            @if ($errors->has('phone'))
                            <div class="alert alert-danger" >
                                @foreach ($errors->get('phone') as $error)
                                {{ $error }}<br>
                                @endforeach
                            </div>
                            @endif

                            <label for="name" >{{ trans('msg.contact.address') }}</label>
                            <textarea cols="30" rows="3" class="dis-block s-text7 size20 bo4 p-l-22 p-r-22 p-t-13 m-b-20" id="address" name="address" placeholder="{{ trans('msg.contact.address') }}">{{ $editprofile->address }}</textarea>
                            @if ($errors->has('address'))
                                <div class="alert alert-danger" >
                                    @foreach ($errors->get('address') as $error)
                                    {{ $error }}<br>
                                    @endforeach
                                </div>
                            @endif


                            <div class="w-size25 m-l-r-auto">
                                <input type="submit" class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4" value="{{ trans('msg.menu_user.my_account.save') }}" />
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6 col-12 p-t-20">
                    <div class="contact_form bo9 card-1 p-t-25 p-l-25  p-r-25  p-b-25">
                        <form method="POST" action="{{ route('user_myaccountCredential') }}" enctype="multipart/form-data">
                            <h3 class="m-text12 m-b-25  t-center">
                                {{ trans('msg.menu_user.my_account.credential') }}
                            </h3>

                            @if(Session::has('success-cred'))
                            <div class="alert alert-success" >
                                {{ Session::get('success-cred') }}
                            </div>
                            @endif

                            {{ csrf_field() }}

                            <div class="p-t-5">
                                <label for="email" >{{ __('msg.auth.email') }}</label>
                                <div class="bo4 of-hidden size15 m-b-5">
                                    <input id="email" type="email" placeholder="{{ __('msg.auth.email') }}" class="sizefull s-text7 p-l-22 p-r-22 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $editprofile->email }}" >
                                </div>
                                @if ($errors->has('email'))
                                    <div class="alert alert-danger" >
                                        @foreach ($errors->get('email') as $error)
                                        {{ $error }}<br>
                                        @endforeach
                                    </div>
                                @endif
                            </div>

                            <div class="p-t-5">
                                <label for="password" >{{ __('msg.auth.password') }}</label>
                                <div class="bo4 of-hidden size15 m-b-5">
                                    <input id="password" type="password" placeholder="{{ __('msg.auth.password') }}"
                                           class="sizefull s-text7 p-l-22 p-r-22 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" >
                                </div>
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger" >
                                        @foreach ($errors->get('password') as $error)
                                        {{ $error }}<br>
                                        @endforeach
                                    </div>
                                @endif
                            </div>

                            <div class="p-t-5 m-b-20">
                                <label for="password_confirmation" >{{ __('msg.auth.password_confirmation') }}</label>
                                <div class="bo4 of-hidden size15 m-b-5">
                                    <input id="password_confirmation" type="password" placeholder="{{ __('msg.auth.password_confirmation') }}"
                                           class="sizefull s-text7 p-l-22 p-r-22 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password_confirmation" >
                                </div>
                                @if ($errors->has('password_confirmation'))
                                <div class="alert alert-danger" >
                                    @foreach ($errors->get('password_confirmation') as $error)
                                    {{ $error }}<br>
                                    @endforeach
                                </div>
                                @endif
                            </div>

                            <div class="w-size25 m-l-r-auto">
                                <input type="submit" class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4" value="{{ trans('msg.menu_user.my_account.save') }}" />
                            </div>

                        </form>
                    </div>
                </div>

        </div>
    </div>
</section>


@endsection


