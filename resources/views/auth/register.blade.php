@extends('app_auth')
@section('title', __('msg.auth.register'))
@section('content')

<div class="container">
    <div class="row justify-content-center">

        <div class="card card-4 col-md-4 m-t-0 m-b-0 p-b-40">

            <h4 class="m-text26 p-b-5 p-t-30 t-center">
                <a href="{{ route('home')}}" >
                    <img src="{{ asset('images/icons/logo.png') }}" alt="Logo">
                </a>
            </h4>

            <h4 class="m-text26 p-b-20 p-t-20 t-center">
                {{ __('msg.auth.register') }}
            </h4>

            <div class="row p-b-10 p-t-10 bo3">
                <div class="col-12 col-md-12 col-lg-12">
                    <h6 class="s-text15 t-center">
                        {{ __('msg.auth.register_social') }}
                    </h6>
                </div>
            </div>

            <div class="row p-b-30">
                <div class="col-6 col-md-6 col-lg-6">
                    <a  href="{{ url('/login/facebook') }}" class="flex-c-m size2 bg_facebook bo-rad-5 hov1 m-text3 trans-0-4">
                        <i class="fa fa-facebook-official" style="font-size: 28px;margin-right: 2px"></i>
                        Facebook
                    </a>
                </div>
                <div class="col-6 col-md-6 col-lg-6">
                    <a  href="{{ url('/login/google') }}" class="flex-c-m size2 bg_google bo-rad-5 hov1 m-text3 trans-0-4">
                        <img src="{{ asset('images/icons/icon-google.png') }}" style="margin-right: 2px" alt="GOOGLE">
                        Google
                    </a>
                </div>
            </div>

            <div class="row p-b-10 p-t-10 bo3">
                <div class="col-12 col-md-12 col-lg-12">
                    <h6 class="s-text15 t-center">
                        {{ __('msg.auth.register') }}
                    </h6>
                </div>
            </div>

            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div>
                    <label for="name" >{{ __('msg.auth.name') }}</label>
                    <div class="bo4 of-hidden size15 m-b-5">
                        <input id="email" type="text" placeholder="{{ __('msg.auth.name') }}" class="sizefull s-text7 p-l-22 p-r-22 form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"  autofocus>
                    </div>
                    @if ($errors->has('name'))
                    <div class="error-form">
                        <strong>{{ $errors->first('name') }}</strong>
                    </div>
                    @endif
                </div>

                <div class="p-t-5">
                    <label for="email" >{{ __('msg.auth.email') }}</label>
                    <div class="bo4 of-hidden size15 m-b-5">
                        <input id="email" type="email" placeholder="{{ __('msg.auth.email') }}" class="sizefull s-text7 p-l-22 p-r-22 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" >
                    </div>
                    @if ($errors->has('email'))
                    <div class="error-form">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                    @endif
                </div>

                <div class="p-t-5">
                    <label for="password" >{{ __('msg.auth.password') }}</label>

                    <div class="bo4 of-hidden size15 m-b-5">
                        <input id="password" type="password" placeholder="{{ __('msg.auth.password') }}" class="sizefull s-text7 p-l-22 p-r-22  form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >
                    </div>
                    @if ($errors->has('password'))
                    <div class="error-form" >
                        <strong>{{ $errors->first('password') }}</strong>
                    </div>
                    @endif
                </div>

                <div class="p-t-10 p-b-15">
                    <label for="password_confirmation" >{{ __('msg.auth.password_confirmation') }}</label>

                    <div class="bo4 of-hidden size15 m-b-5">
                        <input id="password_confirmation" type="password" placeholder="{{ __('msg.auth.password_confirmation') }}" class="sizefull s-text7 p-l-22 p-r-22  form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" >
                    </div>
                    @if ($errors->has('password_confirmation'))
                    <div class="error-form" >
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </div>
                    @endif
                </div>

                <div class="row justify-content-center">
                    <div class="w-size25 ">
                        <!-- Button -->
                        <button  type="submit" class="flex-c-m size2 bg1 bo-rad-5 hov1 m-text3 trans-0-4">
                            {{ __('msg.auth.register') }}
                        </button>
                    </div>
                </div>
            </form>

            <div class="row justify-content-center p-t-10 m-t-10">
                <div class="form-group row mb-0">
                    <a class="header-wrapicon1 dis-block" href="{{ route('password.request') }}">
                        {{ __('msg.auth.forgot') }}
                    </a>
                </div>
            </div>

            <div class="row justify-content-center m-t-5">
                <div class="form-group row mb-0">
                    <a class="header-wrapicon1 dis-block" href="{{ route('login') }}">
                        {{ __('msg.auth.login') }}
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
