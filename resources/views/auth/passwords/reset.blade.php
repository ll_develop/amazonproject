@extends('app_auth')
@section('title', 'Reset Password')
@section('content')

<div class="container">
    <div class="row justify-content-center">

        <div class="card card-4 col-md-4 m-t-0 m-b-0 p-b-40">
            <h4 class="m-text26 p-b-5 p-t-50 t-center">
                <a href="{{ route('home')}}" >
                    <img src="{{ asset('images/icons/logo.png') }}" alt="Logo">
                </a>
            </h4>

            <h4 class="m-text26 p-b-20 p-t-20 t-center">
                {{ __('msg.auth.password_reset') }}
            </h4>

            <div class="row p-b-10 p-t-10 bo3">
                <div class="col-12 col-md-12 col-lg-12">
                    <h6 class="s-text15 t-center">
                        {{ __('msg.auth.password_reset') }}
                    </h6>
                </div>
            </div>

            <form method="POST" action="{{ route('password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="p-t-5">
                    <label for="email" >{{ __('msg.auth.email') }}</label>
                    <div class="bo4 of-hidden size15 m-b-5">
                        <input id="email" type="email" placeholder="{{ __('msg.auth.email') }}" class="sizefull s-text7 p-l-22 p-r-22 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  autofocus>
                    </div>
                    @if ($errors->has('email'))
                    <div class="error-form">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                    @endif
                </div>

                <div class="p-t-5">
                    <label for="password" >{{ __('msg.auth.password') }}</label>

                    <div class="bo4 of-hidden size15 m-b-5">
                        <input id="password" type="password" placeholder="{{ __('msg.auth.password') }}" class="sizefull s-text7 p-l-22 p-r-22  form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >
                    </div>
                    @if ($errors->has('password'))
                    <div class="error-form" >
                        <strong>{{ $errors->first('password') }}</strong>
                    </div>
                    @endif
                </div>

                <div class="p-t-10 p-b-15">
                    <label for="password_confirmation" >{{ __('msg.auth.password_confirmation') }}</label>

                    <div class="bo4 of-hidden size15 m-b-5">
                        <input id="password_confirmation" type="password" placeholder="{{ __('msg.auth.password_confirmation') }}" class="sizefull s-text7 p-l-22 p-r-22  form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" >
                    </div>
                    @if ($errors->has('password_confirmation'))
                    <div class="error-form" >
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </div>
                    @endif
                </div>

                <div class="row justify-content-center p-t-15">
                    <div class="">
                        <button  type="submit" class="flex-c-m size12 bg1 bo-rad-5 hov1 m-text3 trans-0-4">
                            {{ __('msg.auth.password_reset_btn') }}
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
</div>
@endsection
