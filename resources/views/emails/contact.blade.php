<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email</title>
</head>
<body>

<table>
    <tr class="row">
        <td class="col">name</td>
        <td class="col">{{ $name }}</td>
    </tr>
    <tr class="row">
        <td class="col">email</td>
        <td class="col">{{ $email }}</td>
    </tr>
    <tr class="row">
        <td class="col">phone</td>
        <td class="col">{{ $phone }}</td>
    </tr>
    <tr class="row">
        <td class="col">msg</td>
        <td class="col">{{ $msg }}</td>
    </tr>
</table>

</body>
</html>
