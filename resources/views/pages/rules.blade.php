@extends('app')
@section('title', __('msg.header.menu.rules'))
@section('content')

<section class="banner bgwhite p-t-10 p-b-0">
    <div class="container">
        <div class="sec-title p-b-10">
            <h3 class="m-text16 t-center">
                {{ trans('rules.rules_') }}
            </h3>
        </div>
        <div class="row">
            <div class="col-md-6 col-12 p-t-20 p-b-20">
                <div class="contact_form bo9 card-2 p-t-15 p-l-25  p-r-25  p-b-15" style="background-color: white;">
                    <h3 class="m-text14 m-b-5  t-center">
                        {{ trans('rules.partecipate_quest') }}
                    </h3>


                    <div class="p-t-15 p-b-15 bo10b">
                        <span class="m-text14" style="font-size: 36px;">
                           1.
                        </span>
                        <span style="font-size: 24px;">
                           {{ trans('rules.step_1') }}
                            <a href="{{ route('register') }}" style="font-size: 20px;">
                                <u>{{ trans('msg.header.menu.register') }}</u>
                            </a>
                        </span>
                    </div>

                    <div class="p-t-15 p-b-15 bo10b">
                        <span class="m-text14" style="font-size: 36px;">
                           2.
                        </span>
                        <span style="font-size: 24px;">
                           {{ trans('rules.step_2') }}
                        </span>
                    </div>

                    <div class="p-t-15 p-b-15 bo10b">
                        <span class="m-text14" style="font-size: 36px;">
                           3.
                        </span>
                        <span style="font-size: 24px;">
                           {!! trans('rules.step_3') !!}
                        </span>
                    </div>

                    <div class="p-t-15 p-b-15 bo10b">
                        <span class="m-text14" style="font-size: 36px;">
                           4.
                        </span>
                        <span style="font-size: 24px;">
                           {!! trans('rules.step_4') !!}
                        </span>
                    </div>

                    <h3 class="m-text16 m-b-5  t-center">
                        {{ trans('rules.all_here') }}
                    </h3>

                    <img src="{{ asset('images/ui/lucky.jpg') }}" class="img-fluid">
                </div>
            </div>

            <div class="col-md-6 col-12 p-t-10 m-b-50">
                <div class="contact_form bo9 card-2 p-t-15 p-l-25  p-r-25 m-t-20 m-b-20">
                    <img src="{{ asset('images/ui/gift.jpg') }}" class="img-fluid">
                    <h3 class="m-text14 m-b-5  t-center" style="font-size: 24px;">
                        {!! trans('rules.gift') !!}
                    </h3>
                    <h3 class="m-text15 m-b-5 t-center" style="font-size: 22px;">
                        {!! trans('rules.gift_list') !!}
                    </h3>
                </div>

                <div class="contact_form bo9 card-2 p-t-15 p-l-25  p-r-25 m-t-30 m-b-30">
                    <h3 class="m-text14 m-b-15  t-center">
                        {{ trans('rules.important') }}
                    </h3>

                    <h3 class="m-text15 m-b-15 t-left">
                        {!! trans('rules.important_1') !!}
                    </h3>

                    <h3 class="m-text15 m-b-15 t-left">
                        {!! trans('rules.important_2') !!}
                    </h3>

                </div>
                <div class="contact_form bo9 card-2 p-t-15 p-l-25  p-r-25 p-b-25 m-b-50">
                    <h3 class="m-text14 m-b-5  t-center">
                        {{ trans('rules.rules') }}
                    </h3>

                    <div class="p-t-5 p-b-5 bo10b">
                        <span class="m-text14" style="font-size: 24px;">
                           .
                        </span>
                        <span style="font-size: 14px;">
                           {{ trans('rules.rules_1') }}
                        </span>
                    </div>

                    <div class="p-t-5 p-b-5 bo10b">
                        <span class="m-text14" style="font-size: 24px;">
                           .
                        </span>
                        <span style="font-size: 14px;">
                          {{ trans('rules.rules_2') }}
                        </span>
                    </div>

                    <div class="p-t-5 p-b-5 bo10b">
                        <span class="m-text14" style="font-size: 24px;">
                           .
                        </span>
                        <span style="font-size: 14px;">
                            {{ trans('rules.rules_3') }}
                        </span>
                    </div>

                    <div class="p-t-5 p-b-5 bo10b">
                        <span class="m-text14" style="font-size: 24px;">
                           .
                        </span>
                        <span style="font-size: 14px;">
                           {{ trans('rules.rules_4') }}
                        </span>
                    </div>
                    <div class="p-t-5 p-b-5 bo10b">
                        <span class="m-text14" style="font-size: 24px;">
                           .
                        </span>
                        <span style="font-size: 14px;">
                           {{ trans('rules.rules_5') }}
                        </span>
                    </div>
                    <div class="p-t-5 p-b-5 bo10b">
                        <span class="m-text14" style="font-size: 24px;">
                           .
                        </span>
                        <span style="font-size: 14px;">
                           {{ trans('rules.rules_6') }}
                        </span>
                    </div>
                    <div class="p-t-5 p-b-5 bo10b">
                        <span class="m-text14" style="font-size: 24px;">
                           .
                        </span>
                        <span style="font-size: 14px;">
                           {{ trans('rules.rules_7') }}
                        </span>
                    </div>
                    <div class="p-t-5 p-b-5 bo10b">
                        <span class="m-text14" style="font-size: 24px;">
                           .
                        </span>
                        <span style="font-size: 14px;">
                           {{ trans('rules.rules_8') }}
                        </span>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>


<style>
    .container_rules {
        background-image: url("{{ asset('images/ui/rules_rear.jpg') }}");
        background-repeat: no-repeat;
    }
</style>
@endsection
