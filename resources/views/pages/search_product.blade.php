@extends('app')
@section('title', 'Search Product')
@section('content')

@include('fragments.alert_click')

<!-- Banner -->
<section class="banner bgwhite p-t-5 p-b-5">
    <div class="container">
        <div class="row p-b-2 p-t-0 bo7">
            <div class="col-sm-10 col-md-8 col-lg-10">
                <div class="wrap-tags flex-w ">
                    <h4 class="s-text15 p-t-0 m-l-10"> {{ trans('msg.search.asin_search') }}</h4>
                </div>
            </div>
        </div>
    </div>
</section>

@if($errorSearch)
<section class="banner bgwhite p-t-40 p-b-40">
    <div class="container">
        <div class="row">
            <div class="col-sm12 col-md-12 col-lg-12 m-l-r-auto">
                <h4 class="m-text23 p-t-0 p-b-10">
                    {{ trans('msg.search.error_empty.f1') }} "{{ session('search:string') ? session('search:string') : '' }}" {{ trans('msg.search.error_empty.f2') }}
                </h4>

                <h4 class="m-text18 p-t-10 p-b-20">
                    {{ trans('msg.search.error_empty.f3') }}
                </h4>

                <ul>
                    <li>{{ trans('msg.search.error_empty.f4') }}</li>
                    <li>{{ trans('msg.search.error_empty.f5') }}</li>
                </ul>

                @if($errorOutput)
                <h4 class="s-text9 p-t-10 p-b-20">
                    {!! $errorOutput !!}
                </h4>
                @endif


            </div>
        </div>
    </div>
</section>
@else
<section class="banner bgwhite ">
    <div class="container">
        <div class="col-sm-10 col-md-8 col-lg-10">
            <div class="wrap-tags flex-w ">
                <h4 class="s-text15">{{ $pages['totalresults'] }}  {{ trans('msg.search.products') }} | </h4>
                <h4 class="s-text15"> {{ trans('msg.search.page') }} {{ $pages['currentPages'] }} / {{ $pages['totalMaxPages'] }} {{ trans('msg.search.pages') }}</h4>
            </div>
        </div>
    </div>
</section>

<!-- Banner -->
<section class="banner bgwhite p-t-0 p-b-40">
    <div class="container">
        @if($records && count($records) > 0)
        @foreach ($records as $productAmazon)
        <div class="p-t-20 bo7">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-1 col-lg-1">
                    <a href="{{ @$productAmazon->DetailPageURL }}" onclick="genCd({{ @$productAmazon->getAsin() }})" target="_blank" class="flex-sa-m size7b s-text14b bg9 hov1 trans-0-4">
                        <i class="fs-22 fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-12 col-sm-12 col-md-11 col-lg-11">
                    <a href="{{ @$productAmazon->DetailPageURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')" >
                        <h5 class="m-text19 p-b-24">
                            {{ $productAmazon->getName() }}
                        </h5>
                    </a>
                </div>
            </div>

            <div class="row">

                <div class="col-12 col-sm-4 col-md-4 col-lg-4">
                    @if(@$productAmazon->LargeImage)
                    <div class="block1 hov-img-zoom pos-relative m-b-30">
                        <img src="{{ @$productAmazon->LargeImage }}" height="50%">

                        <div class="block2-overlay trans-0-4">
                            <a href="{{ @$productAmazon->AddToWishListURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="block1-wrapbtn w-size2">
                            <a href="{{ @$productAmazon->DetailPageURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')" class="flex-c-m size2 s-text14b bg9 hov1 trans-0-4">
                                {{ trans('msg.search.item.goToAmazon') }}
                            </a>
                        </div>
                    </div>
                    @endif
                </div>

                <div class="col-12 col-sm-8 col-md-8 col-lg-8">

                    <div class="flex-w flex-sb-m p-b-12">
                                <span class="s-text18 w-size18 w-full-sm">
                                    {{ trans('msg.search.item.price') }}
                                </span>
                        <span class="m-text18 w-size16 w-full-sm">
                                    {{ $productAmazon->OfferSummaryFormattedPrice }}
                                </span>
                    </div>

                    <div class="flex-w flex-sb-m p-b-12">
                                <span class="s-text18 w-size18 w-full-sm">
                                    {{ trans('msg.search.item.categoria') }}
                                </span>
                        <span class="m-text18 w-size16 w-full-sm">
                                <a href="{{ route('get_category',$productAmazon->CategoriasId)}}">
                                    <u> {{ @$productAmazon->Categorias }}</u>
                                </a>
                            </span>
                    </div>

                    <div class="wrap-dropdown-content  p-t-15 p-b-14">
                        <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                            {{ trans('msg.search.item.details') }}:
                            <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                            <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                        </h5>

                        <div class="dropdown-content dis-none p-t-15 p-b-23" style="display: none;">
                            <p class="s-text8">
                                @if(@$productAmazon->Feature)
                                @if(!is_array(@$productAmazon->Feature))
                                {!! $productAmazon->Feature !!}
                                @else
                                @if(count(@$productAmazon->Feature) > 0)
                                @foreach (@$productAmazon->Feature as $feature)
                                {!! $feature !!}<br>
                                @endforeach
                                @endif
                                @endif
                                @endif
                            </p>
                        </div>
                    </div>

                    @if($productAmazon->EditorialReview)
                    <div class="wrap-dropdown-content  p-t-15 p-b-14">
                        <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                            {{ trans('msg.search.item.description') }}:
                            <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                            <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                        </h5>

                        <div class="dropdown-content dis-none p-t-15 p-b-23" style="display: none;">
                            <p class="s-text8">
                                {!! @$productAmazon->EditorialReview !!}
                            </p>
                        </div>
                    </div>
                    @endif

                    <div class="wrap-dropdown-content  p-t-15 p-b-14">
                        <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                            {{ trans('msg.search.item.similarproducts') }}:
                            <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                            <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                        </h5>


                        <div class="dropdown-content dis-none p-t-15 p-b-23" style="display: none;">
                            @if(@$productAmazon->SimilarProducts)
                            @if(count(@$productAmazon->SimilarProducts) > 0)
                            @foreach (@$productAmazon->SimilarProducts as $key=>$similarProducts)
                            @if(is_array($similarProducts))
                            @if($similarProducts['Title'] &&  $similarProducts['ASIN'] && $key < 5)
                            <div class="row bo7 m-b-20">
                                <div class="col-1">
                                    <a href="{{ route('get_product',$similarProducts['ASIN'])}}"  class="flex-sa-m size7b s-text14b hov1 trans-0-4">
                                        <i class="fs-22 fa fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="col-11">
                                    <a href="{{ route('get_product',$similarProducts['ASIN'])}}"  >
                                        <h5 class="m-text11b p-b-14">
                                            {{ $similarProducts['Title'] }}
                                        </h5>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @else
                            @if(@$productAmazon->SimilarProducts['Title'] &&  @$productAmazon->SimilarProducts['ASIN'] && $key < 1)
                            <div class="row bo7 m-b-20">
                                <div class="col-1">
                                    <a href="{{ route('get_product',@$productAmazon->SimilarProducts['ASIN'])}}" target="_blank" class="flex-sa-m size7b s-text14b hov1 trans-0-4">
                                        <i class="fs-22 fa fa-arrow-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="col-11">
                                    <a href="{{ route('get_product',@$productAmazon->SimilarProducts['ASIN'])}}" target="_blank" >
                                        <h5 class="m-text11b p-b-14">
                                            {{ @$productAmazon->SimilarProducts['Title'] }}
                                        </h5>
                                    </a>
                                </div>
                            </div>
                            @endif
                            @endif
                            @endforeach
                            @endif
                            @endif
                        </div>
                    </div>

                    <div class="flex-w flex-sb p-t-20">
                        <div class="p-b-4">
                            <span class="s-text8 m-r-25">ASIN: {{ @$productAmazon->getAsin() }}</span>
                            <span class="s-text8 m-r-25">Brand: {{ @$productAmazon->Brand }}</span>
                            <span class="s-text8">Sales Rank: {{ @$productAmazon->SalesRank }}</span>
                        </div>

                    </div>

                    <div class="flex-l-m flex-w p-t-10">
                        <div class="w-size32 flex-m flex-w">
                            <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                                <a href="{{ route('get_product',@$productAmazon->getAsin())}}">
                                    <button class="flex-c-m size8 bg7 hov1 s-text14 trans-0-4" >
                                        <i class="fs-13 fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </a>
                            </div>
                            <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                                <a href="{{ @$productAmazon->AddToWishListURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')">
                                    <button class="flex-c-m size8 bg7 hov1 s-text14 trans-0-4">
                                        <i class="fs-13 fa fa-heart" aria-hidden="true" ></i>
                                    </button>
                                </a>
                            </div>
                            <div class="btn-addcart-product-detail size9b trans-0-4 m-t-10 m-b-10 m-r-10">
                                <a href="{{ @$productAmazon->AllCustomerReviewsURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')">
                                    <button class="flex-c-m size9b bg7 hov1 s-text14 trans-0-4">
                                        {{ trans('msg.search.item.review') }}
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        @endforeach
        @endif
    </div>
</section>

<section class="banner bgwhite bo7">
    <div class="container t-center">
        <div class="col-sm-10 col-md-8 col-lg-10 p-b-5">
            <div class="wrap-tags flex-w ">
                <h4 class="s-text15">{{ $pages['totalresults'] }}  {{ trans('msg.search.products') }} | </h4>
                <h4 class="s-text15"> {{ trans('msg.search.page') }} {{ $pages['currentPages'] }} / {{ $pages['totalMaxPages'] }} {{ trans('msg.search.pages') }}</h4>
            </div>
        </div>
        <div class="pagination flex-m flex-w p-b-26">
            @for ($i = 1; $i <= $pages['totalpages']; $i++)
            <a href="{{ route('search').'?page='.$i }}" class="item-pagination flex-c-m trans-0-4 {{ $pages['currentPages'] == $i ? 'active-pagination' : ''}}">{{$i}}</a>
            @endfor
        </div>
    </div>
</section>

@endif

<hr>
@include('fragments.product_last_search')
<hr>
@include('fragments.product_bestsellers')

@endsection

