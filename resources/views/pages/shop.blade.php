@extends('app')
@section('title', __('msg.header.menu.shop'))
@section('content')

<section class="banner bgwhite p-t-10 p-b-0">
        <div class="container">
            <div class="sec-title p-b-10">
                <h3 class="m-text16 t-center">
                    {{ trans('msg.header.menu.shop') }}
                </h3>
            </div>
            <div class="row">
                Contenuto
            </div>
        </div>
    </section>
@endsection
