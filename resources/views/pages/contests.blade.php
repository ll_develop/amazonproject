@extends('app')
@section('title', __('msg.header.menu.contests'))
@section('content')

<section class="banner bgwhite p-t-10 p-b-0">
        <div class="container container_constest" >
            <div class="sec-title p-b-10">
                <h3 class="m-text16 t-center">
                    {{ trans('msg.header.menu.contests') }}
                </h3>
            </div>
            <div class="row">
                <div class="col-md-6 p-b-0">
                    <div class="leftbar p-t-20 p-r-20 p-r-0-sm">

                        @if($contestActive)

                        <h4 class="m-text14 p-b-1" >
                            {{ trans('msg.menu_user.expire_at') }} :
                        </h4>

                        <h4 class="m-text14 p-b-1 bo10b" style="font-size: 50px;">
                            <div data-countdown="{{ \Carbon\Carbon::parse($contestActive->end)->format('Y/m/d')}}"></div>
                        </h4>

                        <div class="p-t-10 p-b-5">
                            <h4 class="m-text14 p-t-0 p-b-1 " style="font-size: 31px;color: #7a884b;">
                                {{ trans('msg.menu_user.bonus_to_win') }} : {{ $contestActive->gift }}
                            </h4>
                            <h4 class="m-text14 p-t-0 p-b-1 " style="font-size: 14px;color: #525253;">
                                {{ $contestActive->description }}
                            </h4>
                            <img src="{{ asset($contestActive->image) }}" class="img-fluid">
                        </div>


                        <h4 class="s-text21 p-t-0 p-b-7" style="font-size: 16px;color: #424242;">
                            <ul>
                                <li> {{ trans('msg.menu_user.contest_number') }} : {{ $contestActive->name }} </li>
                                <li>
                                    {{ trans('msg.menu_user.start') }}
                                    {{ \Carbon\Carbon::parse($contestActive->start)->format('d/m/Y')}} | {{ trans('msg.menu_user.end') }}
                                    {{ \Carbon\Carbon::parse($contestActive->end)->format('d/m/Y')}}
                                </li>
                                <li> {{ trans('msg.menu_user.exstract_to') }} : {{ \Carbon\Carbon::parse($contestActive->extraction)->format('d/m/Y')}}</li>
                            </ul>
                        </h4>
                        @else
                        <div class="m-text21 active1 p-t-10 p-b-25">
                            {{ trans('msg.menu_user.contest_off') }}
                        </div>
                        @endif
                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </section>

<style>
    .container_constest {
        background-image: url("{{ asset('images/ui/contest_1.jpg') }}");
        background-repeat: no-repeat;
    }
</style>
@endsection



@section('script_page')
<script>
    $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
            $this.html(event.strftime('%D {{ trans('msg.menu_user.days') }} %H:%M:%S'));
        });
    });
</script>

@endsection
