@extends('app')
@section('title', 'Home')
@section('content')


@if($errorSearch)
<section class="banner bgwhite p-t-100 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-sm12 col-md-12 col-lg-12 m-l-r-auto">
                <h4 class="m-text23 p-t-0 p-b-10">
                    {{ trans('msg.search.error_empty.f1c') }}
                </h4>

                @if($errorOutput)
                <h4 class="s-text9 p-t-10 p-b-20">
                    {!! $errorOutput !!}
                </h4>
                @endif

            </div>
        </div>
    </div>
</section>
@else

@include('fragments.alert_click')

<div class="container bgwhite p-t-5 p-b-0">
    <a href="{{ url()->previous() }}" class="flex-sa-m size9 s-text14b hov1 trans-0-4">
        <i class="fs-22 fa fa-arrow-left" aria-hidden="true"></i>{{ trans('msg.back') }}
    </a>
</div>

<div class="container">
<div class="wrap-slick2">
    <div class="slick2">
        @foreach ($categories as $key=>$cat)
        <a href="{{ route('get_category',@$cat->nodeid)}}" >
        <div class="item-slick2 card-1 m-t-20 m-b-20 m-r-10 m-l-10 p-b-50 p-t-50">
            <div class="t-center">
                        <h4>{{@$cat->name}}</h4>
            </div>
        </div>
        </a>
        @endforeach
    </div>
</div>
</div>

<!-- Our product -->
<section class="bgwhite p-t-10 p-b-58">
    <div class="container">
        <div class="sec-title p-b-0">
            <h6 class="s-text5 t-center">
                {{ trans('msg.category.title') }}
            </h6>
            <h3 class="m-text16 t-center">
                {{ $categoryName }}
            </h3>
        </div>

        <!-- Tab01 -->
        <div class="tab01">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#best-seller" role="tab"><h3 class="m-text10 t-left">
                            {{ trans('msg.category.topsellers') }}
                        </h3>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#featured" role="tab">
                        <h3 class="m-text10 t-left">
                            {{ trans('msg.category.mostgifted') }}
                        </h3>
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content p-t-35">
                <!-- - -->
                <div class="tab-pane fade show active" id="best-seller" role="tabpanel">
                    <div class="row">
                        @if($TopSellers && count($TopSellers) > 0)
                            @foreach ($TopSellers as $key=>$productAmazon)
                                <div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
                                    <!-- Block2 -->
                                    <div class="block2">

                                        <div class="block1 hov-img-zoom pos-relative m-b-30">
                                            <img src="{{ @$productAmazon->LargeImage }}" width="50%">
                                            <div class="block1-wrapbtn w-size2 col6">
                                                <a href="{{ @$productAmazon->DetailPageURL }}"
                                                   onclick="genCd('{{ @$productAmazon->getAsin() }}')"
                                                   target="_blank" class="flex-c-m size2 m-text2 bg9 hov1 trans-0-4">
                                                    {{ trans('msg.search.item.goToAmazon') }}
                                                </a>
                                            </div>
                                        </div>

                                    <div class="block2-txt p-t-20">
                                        <a href="{{ route('get_product',@$productAmazon->getAsin())}}" class="block2-name dis-block s-text13 p-b-5">
                                            <b>{{ $key+1 }}.</b> {{@$productAmazon->getName() }}
                                        </a>

                                        <div class="block2-price m-text11 p-r-5 t-left">
                                            {{@$productAmazon->OfferSummaryFormattedPrice }}
                                        </div>
                                    </div>

                                    <div class="flex-l-m flex-w p-t-10">
                                        <div class="w-size32 flex-m flex-w">
                                            <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                                                <a href="{{ route('get_product',@$productAmazon->getAsin())}}">
                                                    <button class="flex-c-m size4 bg7 hov1 s-text14 trans-0-4" >
                                                        <i class="fs-13 fa fa-search" aria-hidden="true"></i>
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                                                <a href="{{ @$productAmazon->AddToWishListURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')">
                                                    <button class="flex-c-m size4 bg7 hov1 s-text14 trans-0-4">
                                                        <i class="fs-13 fa fa-heart" aria-hidden="true" ></i>
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="btn-addcart-product-detail size9b trans-0-4 m-t-10 m-b-10 m-r-10">
                                                <a href="{{ @$productAmazon->AllCustomerReviewsURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')">
                                                    <button class="flex-c-m size4 bg7 hov1 s-text14 trans-0-4">
                                                        {{ trans('msg.search.item.review') }}
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <!-- - -->
                <div class="tab-pane fade" id="featured" role="tabpanel">
                    <div class="row">
                        @if($MostGifted && count($MostGifted) > 0)
                        @foreach ($MostGifted as $key=>$productAmazon)
                        <div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
                            <!-- Block2 -->
                            <div class="block2">

                                <div class="block1 hov-img-zoom pos-relative m-b-30">
                                    <img src="{{ @$productAmazon->LargeImage }}" width="50%">
                                    <div class="block1-wrapbtn w-size2 col6">
                                        <a href="{{ @$productAmazon->DetailPageURL }}"
                                           onclick="genCd('{{ @$productAmazon->getAsin() }}')"
                                           target="_blank" class="flex-c-m size2 m-text2 bg9 hov1 trans-0-4">
                                            {{ trans('msg.search.item.goToAmazon') }}
                                        </a>
                                    </div>
                                </div>

                                <div class="block2-txt p-t-20">
                                    <a href="{{ route('get_product',@$productAmazon->getAsin())}}" class="block2-name dis-block s-text13 p-b-5">
                                        <b>{{ $key+1 }}.</b> {{@$productAmazon->getName() }}
                                    </a>

                                    <div class="block2-price m-text11 p-r-5 t-left">
                                        {{@$productAmazon->OfferSummaryFormattedPrice }}
                                    </div>
                                </div>

                                <div class="flex-l-m flex-w p-t-10">
                                    <div class="w-size32 flex-m flex-w">
                                        <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                                            <a href="{{ route('get_product',@$productAmazon->getAsin())}}">
                                                <button class="flex-c-m size4 bg7 hov1 s-text14 trans-0-4" >
                                                    <i class="fs-13 fa fa-search" aria-hidden="true"></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                                            <a href="{{ @$productAmazon->AddToWishListURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')">
                                                <button class="flex-c-m size4 bg7 hov1 s-text14 trans-0-4">
                                                    <i class="fs-13 fa fa-heart" aria-hidden="true" ></i>
                                                </button>
                                            </a>
                                        </div>
                                        <div class="btn-addcart-product-detail size9b trans-0-4 m-t-10 m-b-10 m-r-10">
                                            <a href="{{ @$productAmazon->AllCustomerReviewsURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')">
                                                <button class="flex-c-m size4 bg7 hov1 s-text14 trans-0-4">
                                                    {{ trans('msg.search.item.review') }}
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>


@endif

<hr>
@include('fragments.product_last_search')
<hr>
@include('fragments.product_bestsellers')

@endsection


