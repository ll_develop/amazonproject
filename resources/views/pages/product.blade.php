@extends('app')
@section('title', 'Home')
@section('content')


@if($errorSearch)
<section class="banner bgwhite p-t-100 p-b-100">
    <div class="container">
        <div class="row">
            <div class="col-sm12 col-md-12 col-lg-12 m-l-r-auto">
                <h4 class="m-text23 p-t-0 p-b-10">
                    {{ trans('msg.search.error_empty.f1b') }}
                </h4>

                @if($errorOutput)
                <h4 class="s-text9 p-t-10 p-b-20">
                    {!! $errorOutput !!}
                </h4>
                @endif

            </div>
        </div>
    </div>
</section>
@else

<!--<div id="alert_code" class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Holy guacamole!</strong> You should check in on some of those fields below.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>-->

<div id="alert_succ" class="alert alert-success" style="display: none">
    <a class="close" style="cursor: pointer" onclick="$('.alert').hide()">×</a>
    <strong>{{ trans('msg.alert.congratulation') }}</strong>
    {{ trans('msg.alert.success.text') }}
    {{ trans('msg.alert.find_code') }} <a href="{{ route('home')}}"><u>{{ trans('msg.alert.codes') }}</u></a>.<br>
    <a href="{{ @$productAmazon->DetailPageURL }}" target="_blank">
        <u>{{ trans('msg.alert.remember') }}</u>
    </a>
</div>

<div id="alert_warn" class="alert alert-warning" style="display: none">
    <a class="close" style="cursor: pointer" onclick="$('.alert').hide()">×</a>
    <strong>{{ trans('msg.alert.congratulation') }}</strong>
    {{ trans('msg.alert.warning.text') }}
    {{ trans('msg.alert.find_code') }} <a href="{{ route('home')}}"><u>{{ trans('msg.alert.codes') }}</u></a>.<br>
    <a href="{{ @$productAmazon->DetailPageURL }}" target="_blank">
        <u>{{ trans('msg.alert.remember') }}</u>
    </a>
</div>

<div class="container bgwhite p-t-0 p-b-0">
    <a href="{{ url()->previous() }}" class="flex-sa-m size9 s-text14b hov1 trans-0-4">
        <i class="fs-22 fa fa-arrow-left" aria-hidden="true"></i>{{ trans('msg.back') }}
    </a>
</div>
<div class="container bgwhite p-t-0 p-b-80">
    <div class="flex-w flex-sb">

        <div class="w-size13 p-t-30 respon5">
            <div class="wrap-slick3 flex-sb flex-w">
                <div class="wrap-slick3-dots"></div>

                <div class="slick3">
                    <div class="item-slick3" data-thumb="{{ $productAmazon->LargeImage }}">
                        <div class="wrap-pic-w">
                            <img src="{{ @$productAmazon->LargeImage }}" style="cursor: pointer">
                        </div>
                    </div>

                    @if($productAmazon->ImageSets &&  count($productAmazon->ImageSets) > 0)
                        @foreach ($productAmazon->ImageSets as $key=>$img)
                            @if(is_array($img) && count($img) >=2 && array_key_exists('LargeImage',$img))
                                @if(@$img['LargeImage'] && @$img['LargeImage']['URL'] && @$img['ThumbnailImage'] && @$img['ThumbnailImage']['URL'] && $key < 5)
                                    <div class="item-slick3" data-thumb="{{ @$img['ThumbnailImage']['URL'] }}">
                                        <div class="wrap-pic-w">
                                            <img src="{{ @$img['LargeImage']['URL'] }}" >
                                        </div>
                                    </div>
                                @endif
                            @else
                                @if(@$productAmazon->ImageSets['LargeImage'] && @$productAmazon->ImageSets['LargeImage']['URL'] && @$productAmazon->ImageSets['ThumbnailImage'] && @$productAmazon->ImageSets['ThumbnailImage']['URL'] && $key < 1)
                                    <div class="item-slick3" data-thumb="{{ @$productAmazon->ImageSets['ThumbnailImage']['URL'] }}">
                                        <div class="wrap-pic-w">
                                            <img src="{{ @$productAmazon->ImageSets['LargeImage']['URL'] }}" >
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    @endif

                </div>
            </div>
        </div>

        <div class="w-size14 p-t-30 respon5">
            <a href="{{ @$productAmazon->DetailPageURL }}" target="_blank" >
            <h4 class="product-detail-name m-text15 p-b-13">

                {{ @$productAmazon->getName() }}

            </h4>
            </a>

            <span class="m-text15 p-b-20">
					{{ @$productAmazon->OfferSummaryFormattedPrice }}
            </span>

            <div class="flex-w flex-sb-m p-t-20  p-b-20">
                        <span class="s-text18 w-size18 w-full-sm">
                            {{ trans('msg.search.item.categoria') }}
                        </span>
                <span class="m-text18 w-size16 w-full-sm">
                    <a href="{{ route('get_category',$productAmazon->CategoriasId)}}">
                        <u> {{ @$productAmazon->Categorias }}</u>
                    </a>
                </span>
            </div>


            <div class="w-size2">
                <a href="{{ @$productAmazon->DetailPageURL }}" onclick="genCd('{{ @$productAmazon->getAsin() }}')" target="_blank" class="flex-c-m size2 s-text14b bg9 hov1 trans-0-4">
                    {{ trans('msg.search.item.goToAmazon') }}
                </a>
            </div>

            <div class="p-t-10">
                <span class="s-text8 m-r-25">ASIN: {{ @$productAmazon->getAsin() }}</span>
                <span class="s-text8 m-r-25">Brand: {{ @$productAmazon->Brand }}</span>
                <span class="s-text8">Sales Rank: {{ @$productAmazon->SalesRank }}</span>
            </div>

            <div class="flex-l-m flex-w p-t-10">
                <div class="w-size32 flex-m flex-w">
                    <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                        <a href="{{ @$productAmazon->AddToWishListURL }}" onclick="genCd('{{ @$productAmazon->getAsin() }}')" target="_blank">
                            <button class="flex-c-m size8 bg7 hov1 s-text14 trans-0-4">
                                <i class="fs-13 fa fa-heart" aria-hidden="true" ></i>
                            </button>
                        </a>
                    </div>
                    <div class="btn-addcart-product-detail size9b trans-0-4 m-t-10 m-b-10 m-r-10">
                        <a href="{{ @$productAmazon->AllCustomerReviewsURL }}" target="_blank" onclick="genCd('{{ @$productAmazon->getAsin() }}')">
                            <button class="flex-c-m size9b bg7 hov1 s-text14 trans-0-4">
                                {{ trans('msg.search.item.review') }}
                            </button>
                        </a>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="wrap-dropdown-content  p-t-15 p-b-14">
        <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
            {{ trans('msg.search.item.details') }}:
            <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
            <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
        </h5>

        <div class="dropdown-content dis-none p-t-15 p-b-23" style="display: none;">
            <p class="s-text8">
                @if(@$productAmazon->Feature)
                @if(!is_array(@$productAmazon->Feature))
                {!! @$productAmazon->Feature !!}
                @else
                @if(count(@$productAmazon->Feature) > 0)
                @foreach (@$productAmazon->Feature as $feature)
                {!! $feature !!}<br>
                @endforeach
                @endif
                @endif
                @endif
            </p>
        </div>
    </div>

    @if($productAmazon->EditorialReview)
    <div class="wrap-dropdown-content  p-t-15 p-b-14">
        <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
            {{ trans('msg.search.item.description') }}:
            <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
            <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
        </h5>

        <div class="dropdown-content dis-none p-t-15 p-b-23" style="display: none;">
            <p class="s-text8">
                {!! $productAmazon->EditorialReview !!}
            </p>
        </div>
    </div>
    @endif

    <div class="wrap-dropdown-content  p-t-15 p-b-14">
        <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
            {{ trans('msg.search.item.similarproducts') }}:
            <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
            <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
        </h5>

        <div class="dropdown-content dis-none p-t-15 p-b-23" style="display: none;">
            @if(@$productAmazon->SimilarProducts)
            @if(count(@$productAmazon->SimilarProducts) > 0)
            @foreach (@$productAmazon->SimilarProducts as $key=>$similarProducts)
            @if(is_array($similarProducts))
            @if($similarProducts['Title'] &&  $similarProducts['ASIN'] && $key < 5)
            <div class="row bo7 m-b-20">
                <div class="col-1">
                    <a href="{{ route('get_product',$similarProducts['ASIN'])}}" class="flex-sa-m size7b s-text14b hov1 trans-0-4">
                        <i class="fs-22 fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-11">
                    <a href="{{ route('get_product',$similarProducts['ASIN'])}}"  >
                        <h5 class="m-text11b p-b-14">
                            {{ $similarProducts['Title'] }}
                        </h5>
                    </a>
                </div>
            </div>
            @endif
            @else
            @if(@$productAmazon->SimilarProducts['Title'] &&  @$productAmazon->SimilarProducts['ASIN'] && $key < 1)
            <div class="row bo7 m-b-20">
                <div class="col-1">
                    <a href="{{ route('get_product',@$productAmazon->SimilarProducts['ASIN'])}}" target="_blank" class="flex-sa-m size7b s-text14b hov1 trans-0-4">
                        <i class="fs-22 fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="col-11">
                    <a href="{{ route('get_product',@$productAmazon->SimilarProducts['ASIN'])}}" target="_blank" >
                        <h5 class="m-text11b p-b-14">
                            {{ @$productAmazon->SimilarProducts['Title'] }}
                        </h5>
                    </a>
                </div>
            </div>
            @endif
            @endif
            @endforeach
            @endif
            @endif
        </div>
    </div>

</div>
</div>

@endif

<hr>
@include('fragments.product_last_search')
<hr>
@include('fragments.product_bestsellers')

@endsection



@section('script_page')
<script type="text/javascript">
    function genCd($cod) {
        var _token = $('[name="_token"]').val();
        console.log("Asin:" + $cod);
        $.ajax({
            url : '{{ url('/product') }}/' + $cod + '/log',
            type : 'post',
            data : { _token : _token },
            success : function(msg){
                //console.log('success');
                //console.log(msg);
                if(msg.new){
                    $('#alert_succ').show();
                    $('#alert_warn').hide();
                    $('#alert_code').html(msg.code);
                }else{
                    $('#alert_warn').show();
                    $('#alert_succ').hide();
                    $('#alert_code').html(msg.code);
                }
            },
            error : function(data){
                //console.log('error');
            }
        });
    };
</script>
@endsection

