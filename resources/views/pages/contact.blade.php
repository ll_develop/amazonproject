@extends('app')
@section('title', __('msg.header.menu.contact'))
@section('content')

<section class="bgwhite p-t-15 p-b-60">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12 p-t-0">
                <div class="contact_form bo9 card-4 p-t-25 p-l-25  p-r-25  p-b-25">
                    <h3 class="m-text16 m-b-25  t-center">
                        {{ trans('msg.header.menu.contact') }}
                    </h3>

                    @if(Session::has('message'))
                        <div class="alert" >
                            {{ Session::get('message') }}
                        </div>
                    @endif

                    <form class="leave-comment" role="form" method="POST" action="{{ route('contact-us') }}" id="formID" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="bo4 of-hidden size15 m-b-20">
                            <input type="text" class="sizefull s-text7 p-l-22 p-r-22" id="name" name="name" value="{{ old('name') }}"  placeholder="{{ trans('msg.contact.name') }} *">
                        </div>

                        @if ($errors->has('name'))
                        <div class="alert alert-danger" >
                            @foreach ($errors->get('name') as $error)
                            {{ $error }}<br>
                            @endforeach
                        </div>
                        @endif

                        <div class="bo4 of-hidden size15 m-b-20">
                            <input type="text" class="sizefull s-text7 p-l-22 p-r-22" id="email" name="email" value="{{ old('email') }}" placeholder="{{ trans('msg.contact.email') }} *">
                        </div>

                        @if ($errors->has('email'))
                        <div class="alert alert-danger">
                            @foreach ($errors->get('email') as $error)
                            {{ $error }}<br>
                            @endforeach
                        </div>
                        @endif

                        <div class="bo4 of-hidden size15 m-b-20">
                            <input type="text" class="sizefull s-text7 p-l-22 p-r-22 " id="phone" name="phone" value="{{ old('phone') }}" placeholder="{{ trans('msg.contact.phone') }}">
                        </div>

                        @if ($errors->has('phone'))
                        <div class="alert alert-danger" >
                            @foreach ($errors->get('phone') as $error)
                            {{ $error }}<br>
                            @endforeach
                        </div>
                        @endif

                            <textarea cols="30" rows="6" class="dis-block s-text7 size20 bo4 p-l-22 p-r-22 p-t-13 m-b-20" id="message" name="message" placeholder="{{ trans('msg.contact.msg') }} *">{{ old('message') }}</textarea>
                            @if ($errors->has('message'))
                            <div class="alert alert-danger" >
                                @foreach ($errors->get('message') as $error)
                                {{ $error }}<br>
                                @endforeach
                            </div>
                            @endif
                        <div class="w-size25">
                            <input type="submit" class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4" value="{{ trans('msg.contact.send') }}" />
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-6 p-b-0">
                <div class="leftbar p-t-50 p-r-20 p-r-0-sm">
                    <!--  -->
                    <h4 class="m-text14 p-b-7">
                        Info
                    </h4>

                    <ul class=" p-t-25 p-b-54">
                        <li class="p-t-4">
                            <div class="s-text13 active1">
                                {{ trans('msg.contact.info_1') }}
                            </div>
                        </li>

                        <li class="p-t-4">
                            <div class="s-text13">
                                {{ trans('msg.contact.mail_1') }}
                            </div>
                        </li>

                        <li class="p-t-4">
                            <div class="s-text13">
                                <hr>
                            </div>
                        </li>

                        <li class="p-t-4">
                            <div  class="s-text13 active1">
                                {{ trans('msg.contact.info_2') }}
                            </div>
                        </li>

                        <li class="p-t-4">
                            <div  class="s-text13">

                                {{ trans('msg.contact.mail_2') }}
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection


