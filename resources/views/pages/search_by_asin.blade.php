@extends('app')
@section('title', __('msg.header.menu.search_by_asin'))
@section('content')

<section class="bgwhite p-t-10 p-b-38">
    <div class="container">
        <div class="sec-title p-b-0">
            <h3 class="m-text16 t-center">
                {{ trans('msg.header.menu.search_by_asin') }}
            </h3>
        </div>
        <div class="p-b-2 p-t-0 bo7">
            <div class="col12">
                <div class="wrap-tags flex-w ">
                    <h4 class="m-text15 p-t-0 m-l-10 t-center">{{  __('msg.search.by_asin') }}</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 p-t-10 p-b-30">
                <div class="bo9 card-4 w-size28 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-auto p-lr-15-sm">
                    <h5 class="m-text20 p-b-24">
                        {{ __('msg.search.search_your_product') }}
                    </h5>

                    <div class="flex-w flex-sb bo10 p-t-15 p-b-20">
                        <div class="w-size28 w-full">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('post_search_by_asin') }}" id="formID" >
                                {{ csrf_field() }}
                                <div class="form-group" >
                                    <label for="asin" class="control-label"> {{  __('msg.search.asin') }}</label>
                                    <div class="size15 bo4 m-b-12">
                                        <input class="sizefull s-text7 p-l-15 p-r-15" type="text" name="asin" value="" max="20">
                                    </div>

                                    @if ($errors->has('asin'))
                                    <div class="alert alert-danger  w-size9  m-t-15">
                                        <ul>
                                            @foreach ($errors->get('asin') as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </div>
                                <div class="w-size2 p-t-5">
                                    <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                                        {{  __('msg.search.find') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="p-t-15 p-b-20">
                        @if(Session::has('success'))
                        <div class="alert alert-success ">
                            {!! Session::get('success') !!}
                        </div>
                        @endif

                        @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {!! Session::get('error') !!}
                        </div>
                        @endif
                    </div>

                </div>
            </div>

            <div class="col-md-8 p-b-30">
                <h3 class="m-text14 p-t-15 p-b-16">
                    {{  __('msg.search.where_find_asin') }}
                </h3>

                <p class="p-b-8">
                    {!!  __('msg.search.response_find_asin') !!}
                </p>

                <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/111891681?autoplay=1&loop=1" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

                <div class="bo13 p-l-29 m-l-9 p-t-50">
                    <h3 class="m-text14 p-t-15 p-b-16">
                        {{  __('msg.search.where_find_asin_1') }}
                    </h3>
                    <img class="img-fluid" src="{{ asset('images/asin/asin_info.png') }}" onclick="openModal();currentSlide(1)" style="cursor: pointer"/>
                    <hr>
                    <h3 class="m-text14 p-t-15 p-b-16">
                        {{  __('msg.search.where_find_asin_2') }}
                    </h3>
                    <img class="img-fluid" src="{{ asset('images/asin/asin_url.png') }}" onclick="openModal();currentSlide(2)" style="cursor: pointer" />
                </div>
            </div>
        </div>
    </div>
</section>



<div id="myModal" class="modal" >
    <span class="close cursor" onclick="closeModal()">&times;</span>
    <div class="modal-content">

        <div class="mySlides">
            <div class="numbertext">1/2</div>
            <img src="{{ asset('images/asin/asin_info.png') }}" style="width:100%">
        </div>
        <div class="mySlides">
            <div class="numbertext">2/2</div>
            <img src="{{ asset('images/asin/asin_url.png') }}" style="width:100%">
        </div>

        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>

        <div class="caption-container">
            <p id="caption"></p>
        </div>

        <div class="row" style="margin-left: 0px;">
            <div class="column">
                <img class="demo" src="{{ asset('images/asin/asin_info.png') }}" style="width:100%" onclick="currentSlide(1)" alt="Find Asin Info" >
            </div>
            <div class="column">
                <img class="demo" src="{{ asset('images/asin/asin_url.png') }}" style="width:100%" onclick="currentSlide(2)" alt="Find Asin URL">
            </div>
        </div>

    </div>
</div>

<style>
    .slick3-dots li {
        display: block;
        position: relative;
        width: 75%;
        margin-bottom: 15px;
    }

    .row > .column {
        padding: 0 8px;
    }

    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    .column {
        float: left;
        width: 15%;
    }

    .modal {
        display: none;
        position: fixed;
        z-index: 9999;
        padding-top: 100px;
        left: 0;
        top: 0px;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: #00000094;
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding: 0;
        width: 100%;
        top: 60px;
        max-width: 1200px;
    }

    /* The Close Button */
    .close {
        color: white;
        position: absolute;
        top: 10px;
        right: 25px;
        font-size: 45px;
        font-weight: bold;
        opacity: 1;
    }

    .close:hover,
    .close:focus {
        color: #999;
        text-decoration: none;
        cursor: pointer;
    }

    /* Hide the slides by default */
    .mySlides {
        display: none;
    }

    /* Next & previous buttons */
    .prev,
    .next {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        padding: 16px;
        margin-top: -50px;
        color: white;
        font-weight: bold;
        font-size: 20px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
        -webkit-user-select: none;
    }

    /* Position the "next button" to the right */
    .next {
        right: 0;
        border-radius: 3px 0 0 3px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover,
    .next:hover {
        background-color: rgba(0, 0, 0, 0.8);
    }

    /* Number text (1/3 etc) */
    .numbertext {
        color: #000000;
        font-size: 14px;
        font-weight: bold;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    /* Caption text */
    .caption-container {
        text-align: center;
        padding: 2px 16px;
        font-family: Montserrat-Black;
        font-size: 18px;
        color: #333333;
        line-height: 1.5;
    }

    img.demo {
        opacity: 0.6;
    }

    .active, .demo:hover {
        opacity: 1;
    }

    img.demo.cursor.active {
        opacity: 1;
    }

    img.hover-shadow {
        transition: 0.3s;
    }

    .hover-shadow:hover {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
</style>



@endsection



@section('script_page')
<script>

    function openModal() {
        document.getElementById('myModal').style.display = "block";
    }

    function closeModal() {
        document.getElementById('myModal').style.display = "none";
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        captionText.innerHTML = dots[slideIndex-1].alt;
    }

</script>
@endsection
