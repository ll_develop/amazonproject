@extends('app')
@section('title', __('msg.header.menu.search_by_asin'))
@section('content')




<section class="banner bgwhite p-t-10 p-b-0">
    <div class="sec-title p-b-10">
        <h3 class="m-text16 t-center">
            {{ trans('msg.header.menu.search_by_asin') }}
        </h3>
    </div>
    <h4 class="m-text15 p-t-0 m-l-10 t-center">{{  __('msg.search.by_asin') }}</h4>

        </div>
    </div>
</section>


<section class="cart bgwhite p-t-70 p-b-100">
    <div class="container">
        <div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-auto p-lr-15-sm">
            <h5 class="m-text20 p-b-24">
                {{ __('msg.search.search_your_product') }}
            </h5>

            <div class="flex-w flex-sb bo10 p-t-15 p-b-20">
                <div class="w-size20 w-full">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('product_by_asin') }}" id="formID" >
                        {{ csrf_field() }}
                        <div class="form-group" >
                            <div class="effect1 w-size9">
                                <label for="asin" class="control-label">Asin</label>
                                <input class="s-text7 w-full p-b-5" type="text" name="asin" placeholder="B01DLMD5O6" value="B01DLMD5O6">
                                <span class="effect1-line"></span>
                            </div>
                            @if ($errors->has('asin'))
                            <div class="alert alert-danger  w-size9  m-t-15">
                                <ul>
                                    @foreach ($errors->get('asin') as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
                        <div class="w-size2 p-t-20">
                            <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                                Check
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection
