<!-- Footer -->

<footer class="bg6 p-t-45 p-b-43 p-l-45 p-r-45">
    <div class="flex-w p-b-90">
        <div class="w-size6 p-t-30 p-l-15 p-r-15 respon3">
            <h4 class="s-text12 p-b-30">
                GET IN TOUCH
            </h4>

            <div>
                <p class="s-text7 w-size27">
                    Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
                </p>

                <div class="flex-m p-t-30">
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-facebook"></a>
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-instagram"></a>
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-pinterest-p"></a>
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-snapchat-ghost"></a>
                    <a href="#" class="fs-18 color1 p-r-20 fa fa-youtube-play"></a>
                </div>
            </div>
        </div>

        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    {{ trans('msg.header.menu') }}
                </h4>
                <ul>
                    <li class="p-b-9"><a href="{{ route('rules')}}">{{ trans('msg.header.menu.rules') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('contests')}}">{{ trans('msg.header.menu.contests') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('shop')}}">{{ trans('msg.header.menu.shop') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('search_by_asin')}}">{{ trans('msg.header.menu.search_by_asin') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('contact')}}">{{ trans('msg.header.menu.contact') }}</a></li>
                </ul>

        </div>

        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    {{ trans('msg.header.menu') }}
                </h4>
                <ul>
                    <li class="p-b-9"><a href="{{ route('rules')}}">{{ trans('msg.header.menu.rules') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('contests')}}">{{ trans('msg.header.menu.contests') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('shop')}}">{{ trans('msg.header.menu.shop') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('search_by_asin')}}">{{ trans('msg.header.menu.search_by_asin') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('contact')}}">{{ trans('msg.header.menu.contact') }}</a></li>
                </ul>

        </div>


        <div class="w-size7 p-t-30 p-l-15 p-r-15 respon4">
                <h4 class="s-text12 p-b-30">
                    Account
                </h4>

            @guest
                <ul>
                    <li class="p-b-9"><a href="{{ route('login') }}" >{{ trans('msg.header.menu.login') }}</a></li>
                    <li class="p-b-9"><a href="{{ route('register') }}" >{{ trans('msg.header.menu.register') }}</a></li>
                </ul>
            @else

            @endif

        </div>





        <div class="w-size8 p-t-30 p-l-15 p-r-15 respon3">
            <h4 class="s-text12 p-b-30">
                Newsletter
            </h4>

            <form>
                <div class="effect1 w-size9">
                    <input class="s-text7 bg6 w-full p-b-5" type="text" name="email" placeholder="email@example.com">
                    <span class="effect1-line"></span>
                </div>

                <div class="w-size2 p-t-20">
                    <!-- Button -->
                    <button class="flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4">
                        Subscribe
                    </button>
                </div>

            </form>
        </div>
    </div>

    <div class="t-center p-l-15 p-r-15">
        <div class="t-center s-text8 p-t-20">
            Copyright © 2018 All rights reserved.
        </div>
        <div class="t-center s-text8 p-t-20">
            <a href="https://www.iubenda.com/privacy-policy/34157236" class="iubenda-white iubenda-embed " title="Privacy Policy">Privacy Policy</a> <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
        </div>
    </div>
</footer>

