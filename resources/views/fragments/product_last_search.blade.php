<!-- last searched -->
@if($lastClicked && count($lastClicked) > 0)
<section class="newproduct bgwhite p-t-0 p-b-0">
    <div class="container">
        <div class="sec-title p-t-20 p-b-20">
            <h3 class="m-text16 t-center">
                {{ trans('msg.home.last_click') }}
            </h3>
        </div>

        <!-- Slide2 -->
        <div class="wrap_slick_last_search">
            <div class="slick_last_search">
                @foreach ($lastClicked as $key=>$feat)
                <div class="item-slick2 p-l-15 p-r-15">
                    <!-- Block2 -->
                    <div class="block2">
                        <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew block2-labelsale">
                            <img src="{{ @$feat->LargeImage }}" width="50%">

                            <div class="block2-overlay trans-0-4">
                                <a href="{{ @$feat->AddToWishListURL }}" target="_blank" onclick="genCd('{{ $feat->getAsin() }}')" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                    <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                    <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                </a>

                                <div class="block2-btn-addcart w-size1 trans-0-4">
                                    <a href="{{ @$feat->DetailPageURL }}"
                                       onclick="genCd('{{ @$feat->getAsin() }}')"
                                       target="_blank" class="flex-c-m size2 m-text2 bg9 hov1 trans-0-4">
                                        {{ trans('msg.search.item.goToAmazon') }}
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="block2-txt p-t-20">
                            <a href="{{ route('get_product',$feat->getAsin())}}" class="block2-name dis-block s-text3 p-b-5">
                                {{@$feat->getName() }}
                            </a>

                            <div class="block2-price m-text6 p-r-5 t-left">
                                {{@$feat->OfferSummaryFormattedPrice }}
                            </div>
                        </div>

                        <div class="flex-l-m flex-w p-t-10">
                            <div class="w-size32 flex-m flex-w">
                                <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                                    <a href="{{ route('get_product',@$feat->getAsin())}}">
                                        <button class="flex-c-m size4 bg7 hov1 s-text14 trans-0-4" >
                                            <i class="fs-13 fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </a>
                                </div>
                                <div class="btn-addcart-product-detail size8 trans-0-4 m-t-10 m-b-10 m-r-10">
                                    <a href="{{ @$feat->AddToWishListURL }}" target="_blank" onclick="genCd('{{ @$feat->getAsin() }}')">
                                        <button class="flex-c-m size4 bg7 hov1 s-text14 trans-0-4">
                                            <i class="fs-13 fa fa-heart" aria-hidden="true" ></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif
