<div id="alert_succ" class="alert alert-success" style="display: none">
    <a class="close" style="cursor: pointer" onclick="$('.alert').hide()">×</a>
    <strong>{{ trans('msg.alert.congratulation') }}</strong>
    {{ trans('msg.alert.success.text') }}
    {{ trans('msg.alert.find_code') }} <a href="{{ route('home')}}"><u>{{ trans('msg.alert.codes') }}</u></a>.<br>
    {{ trans('msg.alert.remember') }}
</div>

<div id="alert_warn" class="alert alert-warning" style="display: none">
    <a class="close" style="cursor: pointer" onclick="$('.alert').hide()">×</a>
    <strong>{{ trans('msg.alert.congratulation') }}</strong>
    {{ trans('msg.alert.warning.text') }}
    {{ trans('msg.alert.find_code') }} <a href="{{ route('home')}}"><u>{{ trans('msg.alert.codes') }}</u></a>.<br>
    {{ trans('msg.alert.remember') }}
</div>


@section('script_page')
<script type="text/javascript">
    function genCd($cod) {
        var _token = $('[name="_token"]').val();
        console.log("Asin:" + $cod);
        $.ajax({
            url : '{{ url('/product') }}/' + $cod + '/log',
            type : 'post',
            data : { _token : _token },
            success : function(msg){
                //console.log('success');
                //console.log(msg);
                if(msg.new){
                    $('#alert_succ').show();
                    $('#alert_warn').hide();
                    $('#alert_code').html(msg.code);
                }else{
                    $('#alert_warn').show();
                    $('#alert_succ').hide();
                    $('#alert_code').html(msg.code);
                }
                $('#myBtn').click();
            },
            error : function(data){
                //console.log('error');
            }
        });
    };
</script>
@endsection
