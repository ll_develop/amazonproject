<!-- Header -->
<header class="header1">
    <!-- Header desktop -->
    <div class="container-menu-header">
        <div class="topbar">
            <div class="topbar-social">
                <a href="#" class="topbar-social-item fa fa-facebook"></a>
                <a href="#" class="topbar-social-item fa fa-instagram"></a>
                <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
            </div>

            <span class="topbar-child1">
                    Premio 150 € {{ trans('msg.menu_user.expire_at') }}:<div style="float: right;" data-countdown="2019/01/31"></div>
            </span>

            <div class="topbar-child2">
					<span class="topbar-email">
						Store: AMAZON.it
					</span>
<!--
                <div class="topbar-language rs1-select2">
                    <select class="selection-1" name="time">
                        <option>Eng</option>
                        <option>IT</option>
                    </select>
                </div>-->
            </div>
        </div>

        <div class="wrap_header">
            <!-- Logo -->
            <a href="{{ route('home')}}" class="logo">
                <img src="{{ asset('images/icons/logo.png') }}" alt="Logo">
            </a>

            <!-- Menu -->
            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <div class="col-sm12 col-md-10 col-lg-8">
                            <div class="search-product pos-relative bo4 of-hidden">
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('searchByString') }}" id="formID" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input class="s-text7 size16 p-l-23 p-r-50" type="text" name="search_product"
                                           placeholder="{{ trans('msg.header.menu.search') }}"
                                           value="{{ (session('search:string')) ?  session('search:string') : '' }}"
                                    >

                                    <button id="bntsend" type="submit" class="flex-c-m size5 ab-r-m color1 color0-hov trans-0-4">
                                        <i class="fs-13 fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </div>
                        </div>

                        <li>
                            <a style="cursor: pointer;">{{ trans('msg.header.menu') }}</a>
                            <ul class="sub_menu">
                                <li><a href="{{ route('rules')}}">{{ trans('msg.header.menu.rules') }}</a></li>
                                <li><a href="{{ route('contests')}}">{{ trans('msg.header.menu.contests') }}</a></li>
                                <li><a href="{{ route('get_bestSellers')}}">{{ trans('msg.category.topsellers') }}</a></li>
                                <li><a href="{{ route('search_by_asin')}}">{{ trans('msg.header.menu.search_by_asin') }}</a></li>
                                <li><a href="{{ route('contact')}}">{{ trans('msg.header.menu.contact') }}</a></li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>



            @guest
                <div class="header-icons">
                    <a href="{{ route('login') }}" class="header-wrapicon1 dis-block">
                        {{ trans('msg.header.menu.login') }}
                    </a>
                    <span class="linedivide1"></span>
                    <a href="{{ route('register') }}" class="header-wrapicon1 dis-block">
                        {{ trans('msg.header.menu.register') }}
                    </a>
                </div>
            @else
                <!-- Header Icon -->
                <div class="header-icons">
                    <a href="{{ route('user_dashboard')}}">{{ Auth::user()->name }}</a>
                    <ul class="main_menu">
                        <li>
                            @if(Auth::user()->image)
                                <a><img src="{{ Auth::user()->image }}" class="header-icon1" width="32px"></a>
                            @else
                                <a><img src="{{ asset('images/icons/icon-header-01.png') }}" class="header-icon1" ></a>
                            @endif

                            <ul class="sub_menu">
                                @if(Auth::user()->isAdmin())
                                    <li><a href="{{ route('admin_dashboard')}}">Dashboard</a></li>
                                    <li><a href="{{ route('add_product')}}">Add Product</a></li>
                                @endif
                                    <li><a href="{{ route('user_dashboard')}}">{{ trans('msg.menu_user.dashboard') }}</a></li>
                                    <li><a href="{{ route('user_mycode')}}">{{ trans('msg.menu_user.all_code') }}</a></li>
                                    <li><a href="{{ route('user_myaccount')}}">{{ trans('msg.menu_user.my_account') }}</a></li>
                                <li>
                                    <a onclick="event.preventDefault();document.getElementById('logout-form').submit();" style="cursor: pointer;" >
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </div>
            @endguest



        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
        <!-- Logo moblie -->
        <a href="{{ route('home')}}" class="logo-mobile">
            <img src="{{ asset('images/icons/logo.png') }}" alt="Logo">
        </a>

        <div class="col-sm-6 col-md-7 col-lg-7 d-none d-sm-block">
            <div class="search-product pos-relative bo4 of-hidden">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('searchByString') }}" id="formID_tablet" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input class="s-text7 size16 p-l-23 p-r-50" type="text" name="search_product" placeholder="{{ session('search:string') ?  session('search:string') : trans('msg.header.menu.search') }}">

                    <button id="bntsend_tablet" type="submit" class="flex-c-m size5 ab-r-m color1 color0-hov trans-0-4">
                        <i class="fs-13 fa fa-search" aria-hidden="true"></i>
                    </button>
                </form>
            </div>
        </div>


        <!-- Button show menu -->
        <div class="btn-show-menu">
            <!-- Header Icon mobile -->
            <div class="header-icons-mobile">

                @guest
                    <div class="header-wrapicon2" style="height: 32px;">
                        <img src="{{ asset('images/ui/mobile_login.jpg') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">

                            <ul class="header-cart-wrapitem">
                                <li class="header-cart-item bo10b">
                                    <a href="{{ route('login') }}">{{ trans('msg.header.menu.login') }}</a>
                                </li>
                                <li class="header-cart-item bo10b">
                                    <a href="{{ route('register') }}">{{ trans('msg.header.menu.register') }}</a>
                                </li>
                            </ul>

                            <ul class="header-cart-wrapitem">
                                <li class="header-cart-item">
                                    <div class="header-cart-item-img" style="margin-top: 15px;">
                                        <img src="{{ asset('images/ui/mobile_luck.jpg') }}" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt" style="font-size: 12px;margin-top: 15px;">
                                            {{ trans('msg.header.menu.mobile_title') }}
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                @else
                    <div class="header-wrapicon2">
                        @if(Auth::user()->image)
                            <img src="{{  Auth::user()->image}}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        @else
                            <img src="{{ asset('images/icons/icon-header-01.png') }}" class="header-icon1 js-show-header-dropdown" alt="ICON">
                        @endif

                        <span class="header-icons-noti">10</span>

                        <!-- Header cart noti -->
                        <div class="header-cart header-dropdown">

                            <ul class="header-cart-wrapitem">
                                @if(Auth::user()->isAdmin())
                                    <li class="header-cart-item bo10b"><a href="{{ route('admin_dashboard')}}">Dashboard</a></li>
                                    <li class="header-cart-item bo10b"><a href="{{ route('add_product')}}">Add Product</a></li>
                                @endif
                                <li class="header-cart-item bo10b">
                                    <a href="{{ route('user_dashboard') }}">{{ trans('msg.menu_user.dashboard') }}</a>
                                </li>
                                <li class="header-cart-item bo10b">
                                    <a href="{{ route('user_mycode')}}">{{ trans('msg.menu_user.all_code') }}</a>
                                </li>
                                <li class="header-cart-item bo10b">
                                    <a href="{{ route('user_myaccount')}}">{{ trans('msg.menu_user.my_account') }}</a>
                                </li>
                            </ul>

                            <ul class="header-cart-wrapitem">
                                <li class="header-cart-item">
                                    <div class="header-cart-item-img">
                                        <img src="{{ asset('images/item-cart-03.jpg') }}" alt="IMG">
                                    </div>

                                    <div class="header-cart-item-txt">
                                        <a href="#" class="header-cart-item-name">
                                            Nixon Porter Leather Watch In Tan
                                        </a>

                                        <span class="header-cart-item-info">
                                                1 x $17.00
                                            </span>
                                    </div>
                                </li>
                            </ul>

                            <div class="header-cart-buttons">
                                <div class="header-cart-wrapbtn">

                                </div>
                                <div class="header-cart-wrapbtn">
                                    <a onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4" style="color: #FFF;cursor: pointer;" >
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endguest
            </div>

            <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
            </div>
        </div>


        <div class="col-sm-6 col-md-7 col-lg-7 p-l-0 d-block d-sm-none">
            <div class="search-product pos-relative bo4 of-hidden">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('searchByString') }}" id="formID_mobile" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input class="s-text7 size16 p-l-23 p-r-50" type="text" name="search_product" placeholder="{{ session('search:string') ?  session('search:string') : trans('msg.header.menu.search') }}">

                    <button id="bntsend_mobile" type="submit" class="flex-c-m size5 ab-r-m color1 color0-hov trans-0-4">
                        <i class="fs-13 fa fa-search" aria-hidden="true"></i>
                    </button>
                </form>
            </div>
        </div>

    </div>

    <!-- Menu Mobile -->
    <div class="wrap-side-menu" >
        <nav class="side-menu">
            <ul class="main-menu">
                <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<span class="topbar-child1">
							Free shipping for standard order over $100
						</span>
                </li>

                <li class="item-topbar-mobile p-l-10">
                    <div class="topbar-social-mobile">
                        <a href="#" class="topbar-social-item fa fa-facebook"></a>
                        <a href="#" class="topbar-social-item fa fa-instagram"></a>
                        <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                    </div>
                </li>

                <li class="item-menu-mobile"><a href="{{ route('home')}}">{{ trans('msg.header.menu.home') }}</a></li>
                <li class="item-menu-mobile"><a href="{{ route('rules')}}">{{ trans('msg.header.menu.rules') }}</a></li>
                <li class="item-menu-mobile"><a href="{{ route('contests')}}">{{ trans('msg.header.menu.contests') }}</a></li>
                <li class="item-menu-mobile"><a href="{{ route('get_bestSellers')}}">{{ trans('msg.category.topsellers') }}</a></li>
                <li class="item-menu-mobile"><a href="{{ route('search_by_asin')}}">{{ trans('msg.header.menu.search_by_asin') }}</a></li>
                <li class="item-menu-mobile"><a href="{{ route('contact')}}">{{ trans('msg.header.menu.contact') }}</a></li>
            </ul>
        </nav>
    </div>
</header>
