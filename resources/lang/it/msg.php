<?php

return [

    'back' => 'Torna indietro',

    //MENU
    'header.menu.home' => 'Home',
    'header.menu.search' => 'Search Products...',

    'header.menu' => 'MENU',
    'header.menu.search_by_asin' => 'Ricerca Con ASIN',
    'header.menu.rules' => 'Regolamento',
    'header.menu.contests' => 'Concorso e Vincitori',
    'header.menu.shop' => 'Selezionati da Noi',
    'header.menu.contact' => 'Contatti',
    'header.menu.login' => 'Login',
    'header.menu.register' => 'Registrati',

    'header.menu.mobile_title' => 'Registrati o Accedi ad AmazonToWin per poter partecipare al concorso ogni volta che Clicchi su un nuovo Prodotto.',


    //HOME
    'home.last_click' => 'Ultimi Prodotti Cercati',
    'home.best_seller' => 'BestSellers su AmazonTopWin',
    'home.our_choice' => 'Prodotti in Evidenza',

    //Search Error
    'search.error_empty.f1' => 'La ricerca',
    'search.error_empty.f1b' => 'Il Codice inserito non coincide con alcun prodotto su Amazon.',
    'search.error_empty.f1c' => 'Il Codice inserito non coincide con alcuna Categoria su Amazon.',
    'search.error_empty.f2' => 'non coincide con alcun prodotto.',
    'search.error_empty.f3' => 'Prova qualcosa come',
    'search.error_empty.f4' => '- Utilizzo di termini più generici',
    'search.error_empty.f5' => '- Controllo ortografico',
    //Search
    'search.asin_search' => 'Puoi anche cercare il tuo prodotto attraverso l\'ASIN (Codice Amazon) nell\'apposita sezione di Ricerca.Clicca Qui.',
    'search.by_asin' => 'In questa sezione puoi cercare un prodotto direttamente tramite l\'ASIN ( Codice Amazon )',
    'search.where_find_asin' => 'Dove trovo l\'ASIN (Codice Amazon del Prodotto) ???',
    'search.response_find_asin' => ' -L\'ASIN su PC lo trovi nella URL di Amazon o nella sezione <b>Ulteriori informazioni</b> del Prodotto 
                    <br>-L\'ASIN su APP MOBILE lo trovi nella sezione <b>Dettagli Prodotto</b>.
                    <br>Come riportato nel video e nelle immagini successive.',
    'search.where_find_asin_1' => 'ASIN nella sezione Ulteriori Informazooni',
    'search.where_find_asin_2' => 'ASIN nella URL del Prodotto',
    'search.search_your_product' => 'Cerca il tuo prodotto',
    'search.products' => 'Prodotti',
    'search.page' => 'Pagina',
    'search.pages' => 'Pagine',
    'search.find' => 'Cerca',
    'search.asin' => 'Asin',


    //Product Item
    'search.item.name' => 'Nome',
    'search.item.price' => 'Prezzo',
    'search.item.categoria' => 'Categoria',
    'search.item.goToAmazon' => 'Vai su Amazon',
    'search.item.details' => 'Dettagli',
    'search.item.description' => 'Descrizione',
    'search.item.similarproducts' => 'Prodotti Simili',
    'search.item.salesRank' => 'Sales Rank',
    'search.item.review' => 'Recensioni',
    'search.item.wishlist' => 'Aggiungi Preferiti',


    //ALERT
    'alert.congratulation' => 'Congratulazioni!!!',
    'alert.find_code' => 'Trovi tutti i tuoi codici nella sezione',
    'alert.codes' => ' Codici',
    'alert.success.text' => 'Hai generato un codice valido per la prossima Estrazione!',
    'alert.warning.text' => 'Il tuo click è stato già registrato nel sistema nelle ultime 24h.',
    'alert.remember' => 'Ricordati di acquistare il prodotto su Amazon dal link che hai cliccato!!!',


    //contact
    'contact.name' => 'Nome',
    'contact.email' => 'Email',
    'contact.phone' => 'Telefono',
    'contact.address' => 'Indirizzo',
    'contact.msg' => 'Messaggio',
    'contact.send' => 'Invia',
    'contact.success' => 'Il suo Messaggio è stato inviato correttamente.',
    'contact.mail_1' => 'info@amazontowin.com',
    'contact.mail_2' => 'gift@amazontowin.com',
    'contact.info_1' => 'Per informazioni generali scrivere a quest\'indirizzo:',
    'contact.info_2' => 'Per informazioni riguardo le giftcard o problemi nelle recezione dei Premi Estratti contattaci a quest\'indirizzo:',



    //category
    'category.title' => 'Categoria',
    'category.topsellers' => 'I più Venduti',
    'category.mostgifted' => 'I più Regalati',

    //Auth
    'auth.login' => 'Accedi',
    'auth.register' => 'Registrati',
    'auth.login_social' => 'Accedi con i Social',
    'auth.register_social' => 'Registrati con i Social',
    'auth.name' => 'Nome',
    'auth.email' => 'Email',
    'auth.password' => 'Password',
    'auth.password_confirmation' => 'Conferma la Password',
    'auth.password_reset' => 'Password Reset',
    'auth.password_reset_link' => 'Invia il Link',
    'auth.password_reset_btn' => 'Reset Password',
    'auth.remember_me' => 'Remember Me',
    'auth.forgot' => 'Hai dimenticato la Password?',


    //Menu User
    'menu_user.dashboard' => 'Dashboard',

    //my account
    'menu_user.my_account' => 'I miei Dati',
    'menu_user.my_account.info' => 'Informazioni',
    'menu_user.my_account.credential' => 'Credenziali',
    'menu_user.my_account.save' => 'Salva',
    'menu_user.my_account.success' => 'Account Aggiornato Correttamente!',

    'menu_user.dashboard_data' => 'Creato in Data',
    'menu_user.dashboard_data_' => 'Data',
    'menu_user.days' => 'Giorni',
    'menu_user.description' => 'Descrizione',
    'menu_user.dashboard_code' => 'Codice',
    'menu_user.all_code' => 'Tutti I Codici Generati',
    'menu_user.prod_rel' => 'Generato da questo Prodotto',
    'menu_user.important' => 'Importante',
    'menu_user.important_des' => 'Ti ricordiamo che i codici generati sono validi solo se hai finalizzato l\'acquisto su Amazon del prodotto, se non lo hai fatto puoi farlo cliccando sul link in basso ad ogni codice generato che visualizzerà il prodotto associtato.',
    'menu_user.dashboard_gift_win' => 'Premi Vinti',
    'menu_user.dashboard_last_code' => 'Ultimi Codici Generati',
    'menu_user.dashboard_last_code_empty' => ' Non sono stati trovati Codici generati,puoi iniziare cercando un prodotto e cliccando sul pulsante "Vai su Amazon",puoi consulare la sezione ',
    'menu_user.dashboard_gift_empty' => 'AmazonToWin è Gratuito puoi continuare a generare codici e cercare di vincere i bonus Amazon,puoi iniziare cercando un prodotto e cliccando sul pulsante "Vai su Amazon",puoi consultare la sezione ',

    //CONTEST
    'menu_user.code_generate' => 'Codici Generati Per il Concorso Corrente ',
    'menu_user.code_generate_off' => 'Codici Generati per il Primo Concorso Attivo',
    'menu_user.expire_at' => 'Concorso Attuale Scade Tra',
    'menu_user.exstract_to' => 'Estrazione il',
    'menu_user.bonus_to_win' => 'Puoi vincere',
    'menu_user.contest_number' => 'N°',
    'menu_user.start' => 'Attivo Dal:',
    'menu_user.end' => 'Al:',
    'menu_user.contest_off' => 'Al momento non è attivo nessun concorso tutti codici generati in questo arco di tempo saranno considerati Validi alla riapertura dei concorsi',


];
