<?php

return [

    'partecipate_quest' => 'Come Partecipare all\'Estrazione dei Bonus Amazon?',


    'step_1' => 'Iscriviti al sito AmazonToWin ',
    'step_2' => 'Condividi sui Social AmazonToWin',
    'step_3' => 'Cerca un Prodotto dalla nostra barra di ricerca, selezionalo e Clicca su <b>Acquista su Amazon</b> e acquistalo su <i>Amazon</i>',
    'step_4' => 'Per ogni prodotto Cliccato viene generato un <b>codice</b> per parteciapare al Concorso Corrente,più prodotti vengono Cliccati maggiore è la possibilità di essere Estratti',
    'all_here' => 'Fine,Tutto Qui !!!',

    'important' => 'IMPORTANTE',
    'important_1' => ' -Il Prodotto Linkato tramite il nostro sito <b>DOVRA\' essere acquistato su AMAZON</b>
                        per la corretta partecipazione al concorso',
    'important_2' => '-Tutte le transazioni,gli accessi,spedizioni <b>VENGONO GESTITI DA AMAZON</b>,
                        noi siamo esclusivamente un sito di Referal e Traffico Dati che mette a disposizione premi',

    'gift' => 'PREMI',
    'gift_list' => 'Buoni Amazon con un Valore <br> dalle  50 Euro alle 500 Euro',

    'rules_' => 'Regolamento | AmazonToWin è Gratis !!!',
    'rules' => 'Regolamento',

    'rules_1' => 'La Condivisione sui Social è Sufficiente la prima volta per i successivi Click non è necessaria,
                            ovviamente se vuoi condividere AmazonToWin sei il Benvenuto.',
    'rules_2' => ' I Vincitori Possono decidere se Lasciare un Feedback/Testimonianza Video o Scritta della loro
                            Vittoria che andrà pubblicata nella sezione Vincitori o sulla nostra Pagina Facebook/Instagram.',
    'rules_3' => 'Ogni volta che un Prodotto Viene Cliccato dal pulsante "Acquista su Amazon" generi un codice per
                            il concorso attivo in quel momento.L\'estrazione avviene tra tutti i codici generati tra la data
                            d\'inizio e fine di quel specifico concorso.Quindi è sulla base dei Codici e non degli Utenti',
    'rules_4' => ' L\'estrazione viene fatta tramite un sistema Randomico che genererà il Codice vincente per quel concorso
                            e dal codice vincente si risale a chi lo ha generato proclamando il Vincitore.',
    'rules_5' => ' I risultati verranno pubblicati nella sezione dedicata del sito Vincitori e sui Vari Social
                            Facebook/Instagram con Annuncio dell\'estrazione.',
    'rules_6' => ' I Vincitori non sono unici per concorso.Dipende da come viene impostato il concorso e dai premi in Palio.',
    'rules_7' => 'Il Vincitore sarà ritenuto tale solo nel caso in cui il Codice Vincente Estratto è asscoiato ad un prodotto Amazon che l\'utente ha Acquistato su Amazon e non semplicemente Cliccato',
    'rules_8' => 'Qualora il Vincitore risultasse non valido verrà Estratto un nuovo Codice per quel Concorso,fino a quando non risulterà un Vincitore Valido',
    'rules_9' => 'Regolamento',
    'rules_10' => 'Regolamento',
    'rules_11' => 'Regolamento',
    'rules_12' => 'Regolamento',

];
