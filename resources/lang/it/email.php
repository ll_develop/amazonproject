<?php

return [
    'hello' => 'Ciao, ',
    'welcome' => 'Benvenuto, ',
    'salutation' => 'Saluti,AmazonToWin',


    'reset_pw.subject' => 'Notifica per Reset Password',
    'reset_pw.line_1' => 'Hai ricevuto questa email perché abbiamo ricevuto una richiesta di reimpostazione della password per il tuo account.',
    'reset_pw.btn' => 'Reset Password',
    'reset_pw.line_2' => 'Se non hai richiesto la reimpostazione della password, non sono necessarie da parte tua ulteriori azioni.',

    'welcome.subject' => 'Benvenuto su AmazonToWin',
    'welcome.line_1' => 'You are receiving this email because we received a password reset request for your account.',
    'welcome.btn' => 'Reset Password',
    'welcome.line_2' => 'If you did not request a password reset, no further action is required.',



];
