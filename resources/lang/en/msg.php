<?php

return [

    'back' => 'Torna indietro',

    //MENU
    'header.menu.home' => 'Home',
    'header.menu.search' => 'Search Products...',

    'header.menu' => 'MENU',
    'header.menu.search_by_asin' => 'Ricerca Con ASIN',
    'header.menu.rules' => 'Regolamento',
    'header.menu.contests' => 'Estrazioni e Vincitori',
    'header.menu.shop' => 'Selezionati da Noi',
    'header.menu.contact' => 'Contatti',

    'header.menu.login' => 'Login',
    'header.menu.email' => 'Email',
    'header.menu.password' => 'Password',
    'header.menu.remember_me' => 'Remember Me',
    'header.menu.forgot' => 'Hai dimenticato la Password?',
    'header.menu.register' => 'Registrati',


    //HOME
    'home.last_click' => 'Ultimi Prodotti Cercati',
    'home.best_seller' => 'BestSellers',
    'home.our_choice' => 'Prodotti in Evidenza',

    //Search Error
    'search.error_empty.f1' => 'La ricerca',
    'search.error_empty.f1b' => 'Il Codice inserito non coincide con alcun prodotto su Amazon.',
    'search.error_empty.f2' => 'non coincide con alcun prodotto.',
    'search.error_empty.f3' => 'Prova qualcosa come',
    'search.error_empty.f4' => '- Utilizzo di termini più generici',
    'search.error_empty.f5' => '- Controllo ortografico',
    //Search
    'search.asin_search' => 'Puoi anche cercare il tuo prodotto attraverso l\'ASIN (Codice Amazon) nell\'apposita sezione di Ricerca.Clicca Qui.',
    'search.products' => 'Prodotti',
    'search.page' => 'Pagina',
    'search.pages' => 'Pagine',


    //Product Item
    'search.item.name' => 'Nome',
    'search.item.price' => 'Prezzo',
    'search.item.categoria' => 'Categoria',
    'search.item.goToAmazon' => 'Vai su Amazon',
    'search.item.details' => 'Dettagli',
    'search.item.description' => 'Descrizione',
    'search.item.similarproducts' => 'Prodotti Simili',
    'search.item.salesRank' => 'Sales Rank',
    'search.item.review' => 'Recensioni',
    'search.item.wishlist' => 'Aggiungi Preferiti',


    //ALERT
    'alert.congratulation' => 'Congratulazioni!!!',
    'alert.find_code' => 'Trovi tutti i tuoi codici nella sezione',
    'alert.codes' => ' Codici',
    'alert.success.text' => 'Hai generato un codice valido per la prossima Estrazione!',
    'alert.warning.text' => 'Il tuo click è stato già registrato nel sistema nelle ultime 24h.',
    'alert.remember' => 'Ricordati di acquistare il prodotto su Amazon dal link che hai cliccato!!!',


    //contact
    'contact.name' => 'Nome',
    'contact.email' => 'Email',
    'contact.phone' => 'Telefono',
    'contact.msg' => 'Messaggio',
    'contact.send' => 'Invia',



];
