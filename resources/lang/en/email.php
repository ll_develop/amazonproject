<?php

return [

    'reset_pw.subject' => 'Reset Password Notification',
    'reset_pw.line_1' => 'You are receiving this email because we received a password reset request for your account.',
    'reset_pw.btn' => 'Reset Password',
    'reset_pw.line_2' => 'If you did not request a password reset, no further action is required.',

    'welcome.subject' => 'Benvenuto su AmazonToWin',
    'welcome.line_1' => 'You are receiving this email because we received a password reset request for your account.',
    'welcome.btn' => 'Reset Password',
    'welcome.line_2' => 'If you did not request a password reset, no further action is required.',



];
