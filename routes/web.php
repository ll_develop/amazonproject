<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', function () {return redirect()->route('home');});

Route::get('/rules', 'PageController@rules')->name('rules');
Route::get('/contests', 'PageController@contests')->name('contests');
Route::get('/shop', 'PageController@shop')->name('shop');


Route::get('/contact','PageController@contact')->name('contact');
Route::post('/contact','PageController@mailsend')->name('contact-us');

Route::get('/search/asin', 'PageController@searchByAsin')->name('search_by_asin');
Route::post('/search/asin', 'PageController@searchByAsinPost')->name('post_search_by_asin');

Route::post('/search', 'SearchController@searchByString')->name('searchByString');
Route::get('/search', 'SearchController@search')->name('search');

Route::get('/bestsellers', 'CategoryController@bestSellers')->name('get_bestSellers');
Route::get('/category/{nodeid}', 'CategoryController@getCategory')->name('get_category');

Route::get('/product/{asin}', 'ProductController@getProduct')->name('get_product');

Route::group(['middleware' => ['throttle:100']], function() {
    Route::post('/product/{asin}/log', 'ProductController@loggerOpenProduct')->name('loggerOpenProduct');
});


Route::group(['prefix' => 'admin','namespace' => 'Admin','middleware' => ['auth','web','admin']], function() {
    Route::get('/', 'DashboardController@dashboard')->name('admin_dashboard');

    Route::group(['prefix' => 'product'], function() {
        Route::get('/add', 'AmazonProductController@add')->name('add_product');
        Route::post('/add', 'AmazonProductController@productByAsin')->name('product_by_asin');
        Route::post('/category/add', 'AmazonProductController@categoryByNode')->name('category_by_node');
        Route::post('/addtomydb', 'AmazonProductController@addToMyDb')->name('addToMyDb');
    });

    Route::group(['prefix' => 'contest'], function() {
        Route::get('/', 'AmazonContestController@createContest')->name('admin_create_contest');
        Route::get('/{id}', 'AmazonContestController@updateContest')->name('admin_create_contest');
    });

    Route::group(['prefix' => 'winner'], function() {
        Route::get('/', 'AmazonWinnerController@createWinner')->name('admin_create_winner');
        Route::get('/{id}', 'AmazonWinnerController@updateWinner')->name('admin_create_winner');
    });
});


Route::group(['prefix' => 'user','namespace' => 'User','middleware' => ['auth','web']], function() {
    Route::get('/dashboard', 'DashboardController@index')->name('user_dashboard');
    Route::get('/code', 'UserController@myCode')->name('user_mycode');
    Route::get('/my-account', 'UserController@myAccount')->name('user_myaccount');
    Route::post('/my-account', 'UserController@myaccountEdit')->name('user_myaccountEdit');
    Route::post('/my-account-credential', 'UserController@myaccountCredential')->name('user_myaccountCredential');
    Route::get('/delete-account', 'UserController@deleteaccount')->name('user_deleteaccount');
});


Route::get('login/{provider}', 'SocialController@Redirect');
Route::get('login/{provider}/callback','SocialController@Callback');
Auth::routes(['verify' => true]);

Route::get('/test', 'TestController@test');
Route::get('/test2', 'TestController@test2');




